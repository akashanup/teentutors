<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Mail;
use Laravel\Socialite\Facades\Socialite;

use App\Models\User;
use App\Models\UserSubjectRelationship;
use App\Models\UserGradeRelationship;
use App\Models\Subject;
use App\Models\Grades;
use App\Models\UserRatings;
use App\Models\UserZipcode;
use App\Models\ZipcodeLocation;
use App\Models\ResetPasswordLink;
use App\Models\AccountActivationLink;

use App\Http\Requests\UserRegistrationRequest;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\UserForgetPasswordRequest;
use App\Http\Requests\UserResetPasswordRequest;
use App\Http\Requests\UserContactRequest;
use Carbon\Carbon;


class UserController extends Controller
{
    public function isUser()
    {
        if((Session::has('user_id')))
        {
            return true;
        }      
        return false;
    }

    public function addLatLong($zipcode,$latitude,$longitude)
    {
        $zipcodeLoc                 =   ZipcodeLocation::where('zipcode', $zipcode)->first();
        if(count($zipcodeLoc)==0) 
        {
            $zipcodeLoc             =   new ZipcodeLocation();
            $zipcodeLoc->zipcode    =   $zipcode;
            $zipcodeLoc->latitude   =   $latitude;
            $zipcodeLoc->longitude  =   $longitude;
            $zipcodeLoc->save();
        }
    }

    public function checkZipcode(Request $request)
    {
        $json                   =   [];
        $json['isZipcode']      =   1;
        $zipcodeLoc             =   ZipcodeLocation::where('zipcode', $request->zipcode)->first();
        
        if(count($zipcodeLoc)   ==  0) 
        {
            $json['isZipcode']  =   0;
        }
        return $json;
    }

    public function addZipcode(Request $request)
    {
        $this->addLatLong($request->zipcode,$request->latitude,$request->longitude);    
    }
    

    public function registration(UserRegistrationRequest $request)
    {
        try 
    	{
    		$user 			= 	User::where('email', $request->email)->first();
            if($user) 
            {
                return redirect()->back()->withErrors('Email already registered.')->withInput($request->except('email'));
            }
            if($request->password != $request->cnf_password)
            {
                return redirect()->back()->withErrors("Password and Confirm Password didn't match.")->withInput($request->except('password','cnf_password'));    
            }
            if (filter_var($request->email, FILTER_VALIDATE_EMAIL) === false)
            {
                return redirect()->back()->withErrors("Please provide a valid email address.")->withInput($request->except('email'));
            }
            if (filter_var($request->parents_email, FILTER_VALIDATE_EMAIL) === false)
            {
                return redirect()->back()->withErrors("Please provide a valid parent's email address.")->withInput($request->except('parents_email'));
            }
            if($request->email == $request->parents_email)
            {
                return redirect()->back()->withErrors("Your email and your parent's email can not be same.")->withInput($request->except(['email','parents_email']));
            }
            //if(strlen($request->password)<8)
            //'/^(?=.*\d)(?=.*[@#\-_$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=§!\?]{6,200}$/'
            if(!preg_match('/^(?=.*[A-Z])(?=.*\d)[a-zA-Z\d$@$!%*#?&]{8,200}$/',$request->password))
            {
                return redirect()->back()->withErrors("Password must be of atleast 8 characters with at least one uppercase character and one digit.")->withInput($request->except('password'));
            }
    		
            $user 				= 	new User();
	    	$user->name 		=	$request->name;
	    	$user->email 		=	$request->email;
            $user->parents_email=   $request->parents_email;
            $user->image        =   url('/frontend/images/default.jpg');
            $user->resume       =   url('/frontend/images/default.jpg');
	    	$user->password		=	Hash::make($request->password);
            $user->myGrade_id 	=   $request->myGrade_id;
	    	$user->comments  	=   $request->comments;
	    	$user->role_id 		=	$request->role_id;
            
            if($request->role_id == 1)
            {
                if($request->price > 15 || $request->price <= 0)
                {
                    return redirect()->back()->withErrors('Price should be between $0 - $15.')->withInput($request->except('price'));;   
                }
                $user->price       =   $request->price;
                $user->avgRating   =   0;
            }
            $user->save();

            if($request->role_id == 1)
            {
                if($request->image)
                {
                    if(($request->image->getMimeType() == 'image/png' || $request->image->getMimeType() == 'image/jpeg' || $request->image->getMimeType() == 'image/gif')) 
                    {
                        
                        $ext            =   $request->image->guessExtension();
                        $name           =   "user_image_".$user->id.".$ext";
                        $url            =   url('frontend/images/'.$name);
                        $image          =   Image::make($request->image)->resize(300,300);
                        if($image) 
                        {
                            $image      ->  save("frontend/images/{$name}");
                            $user->Update([
                                'image' => $url
                            ]);
                        }
                        else 
                        {
                            $errors[]   =   'Picture height should not be greater than width. Please select a different image afterwards';    
                            return redirect()->back()->withErrors($errors)->withInput();;
                        }
                    }
                    else 
                    {
                        $errors[]       =   'Picture should be a image';
                        return redirect()->back()->withErrors($errors)->withInput();;
                    }   
                }
                
                if($request->resume)
                {
                    /*
                        if(($request->resume->getMimeType() == 'image/png' || $request->resume->getMimeType() == 'image/jpeg' || $request->resume->getMimeType() == 'image/gif'))
                        {
                            
                            $ext            =   $request->resume->guessExtension();
                            $name           =   "user_resume_".$user->id.".$ext";
                            $url            =   url('frontend/images/'.$name);
                            $image          =   Image::make($request->resume)->resize(600,600);
                            if($image) 
                            {
                                $image      ->  save("frontend/images/{$name}");
                                $user->Update([
                                    'resume' => $url
                                ]);
                            }
                            else 
                            {
                                $errors[]   =   'Invalid resume format.';
                                return redirect()->back()->withErrors($errors);    
                            }
                        }
                        else 
                        {
                            $errors[]       =   'Resume should be an Image';
                            return redirect()->back()->withErrors($errors);
                        }
                    */
                    $ext            =   $request->resume->guessExtension();
                    $name           =   "user_resume_".$user->id.".$ext";
                    $url            =   url('frontend/images/'.$name);   
                    $request->resume->move("frontend/images/","$name");
                    $user->Update([
                        'resume' => $url
                    ]);
                }

                $grd = Grades::whereBetween('id', [$request->from,$request->to])->get();
                foreach ($grd as $key => $value) 
                {
                    $userGradeRelationship                              =   new UserGradeRelationship();
                    $userGradeRelationship->user_id                     =   $user->id;
                    $userGradeRelationship->interestedGrades_id         =   $value->id;
                    $userGradeRelationship->save();
                }
            }
            

            if($request->role_id == 2)
            {
                foreach ($request->interestedGrades_id as $key => $value) 
                {
                    $userGradeRelationship                              =   new UserGradeRelationship();
                    $userGradeRelationship->user_id                     =   $user->id;
                    $userGradeRelationship->interestedGrades_id         =   $value;
                    $userGradeRelationship->save();
                }    
            }            

            foreach ($request->subject_id as $key => $value) 
            {
                $userSubjectRelationship                =   new UserSubjectRelationship();
                $userSubjectRelationship->user_id       =   $user->id;
                $userSubjectRelationship->subject_id    =   $value;
                $userSubjectRelationship->save();
            }

            $zipcode            =   new UserZipcode();
            $zipcode->user_id   =   $user->id;
            $zipcode->zipcode   =   $request->zipcode;
            $zipcode->save();
	    	
            //function to add latitude and longitude
            $this->addLatLong($request->zipcode,$request->latitude,$request->longitude);

            /*
                $zipcodeLoc                 =   ZipcodeLocation::where('zipcode', $request->zipcode)->first();
                if(count($zipcodeLoc)==0) 
                {
                    $zipcodeLoc             =   new ZipcodeLocation();
                    $zipcodeLoc->zipcode    =   $request->zipcode;
                    $zipcodeLoc->latitude   =   $request->latitude;
                    $zipcodeLoc->longitude  =   $request->longitude;
                    $zipcodeLoc->save();
                }
            */
                
            //send a mail activation link to parent.
            $encrypt    =   md5($user->email.$user->parents_email.date("d.m.y").date("H:i:s"));
            $string     =   url('/user/activateAccount/'.$encrypt);
            try {                    
                $forgetpassword                 =   new AccountActivationLink();
                $forgetpassword->user_id        =   $user->id;
                $forgetpassword->unique_key     =   $encrypt;
                $forgetpassword->save();
                
                $privacyPolicy = url('/home/privacyPolicy');
                $termsAndCondition = url('/home/termsOfService');
                
                $this->sendMail($user->parents_email, 'TeenTutors - Account Activation Link!', "User Email Id : {$user->email}<br><a href='{$string}'>Click here to to activate your child's account.</a><br>By confirming your child’s account, you agree to the <a href='{$termsAndCondition}' target='_blank'>terms of service</a> and the <a href='{$privacyPolicy}' target='_blank'>privacy policy</a>.");
                Session::put('activate', "true");
                return redirect("/user/activate");
            } 
            catch (Exception $e) {
                return 'Something went wrong. Please try again.';
            }

            //logging in user if regisstration is successful
            /*Session::put('user_id', $user->id);
            Session::put('user_role', $user->role_id);
            Session::put('user_name', $user->name);*/
            return redirect("/");
	    	
    	}
    	catch (Exception $e) 
    	{
    		return redirect()->back()->withErrors('Something went wrong. Please try again.'); 	
    	}
    }

    public function activate()
    {
        try 
        {
            if(Session::has("activate"))
            {
                Session::forget('activate');
                return View('frontend.activationMessage');      
            }
            else
            {
                return redirect("/");
            }
            
        } 
        catch (Exception $e) {
            
        }
    }

    public function activateAccount($key)
    {
        try {
            $user_id            =   AccountActivationLink::where('unique_key',$key)->first();
            if($user_id)
            {
                $expiry_date    =   $user_id->created_at->addHours(720);
                if(Carbon::now()->lte($expiry_date))
                {
                    $user       =   User::where('id',$user_id->user_id)->first();
                    if($user)
                    {
                        $user_id->UPDATE(array(
                            'created_at'    =>  $user_id->created_at->addHours(-720)
                        ));
                        $user->UPDATE(array(
                            'status'    =>  1
                        ));

                        Session::put('user_id', $user->id);
                        Session::put('user_role', $user->role_id);
                        Session::put('user_name', $user->name);

                        return redirect('/');
                    }
                    else
                    {
                        dd('User not found');
                    }    
                }
                else
                {
                    dd("session-expires");
                }
            }
            else
            {
                return redirect('/');
            }
        } 
        catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');   
        }
    }

    public function login(UserLoginRequest $request)
    {
    	try
        {
            $user 	=	User::where('email', $request->email)->where('users.status',1)->first();
            if($user)
            {
                if(Hash::check($request->password, $user->password))
                {
                    Session::put('user_id', $user->id);
                    Session::put('user_role', $user->role_id);
                    Session::put('user_name', $user->name);
                    return;
                }
                else
                {
                    return 'Username and password does not match.';
                }
            }
            else
            {
            	return 'User not found.';	
            }
        }
        catch(Exception $e)
        {
            return 'Something went wrong. Please try again.';
        }
    }

    public function logout()
    {
        Session::forget('user_id');
        Session::forget('user_role');
        Session::forget('user_name');
        return redirect('/');
    }

    public function deleteProfile($id)
    {
        $user   =   User::where('id',$id)->where('users.status',1)->delete();
        Session::forget('user_id');
        Session::forget('user_role');
        Session::forget('user_name');
    }

    public function myProfile()
    {
        try 
        {
            if(!$this->isUser())
            {
                return redirect('/');
            }
            $user                       =   User::where('users.id',Session::get('user_id'))->where('users.status',1)
                                                ->Join('grades','grades.id','=','users.myGrade_id')
                                                ->join('roles','roles.id','=','users.role_id')
                                                ->join('user_zipcodes','user_zipcodes.id','=','users.id')
                                                ->select('users.*','user_zipcodes.zipcode','grades.grade as userGrade','grades.id as gradeId','roles.role as userRole')
                                                ->first();
            if(!$user)
            {
                return redirect('/');
            }

            $userSubjectRelationship    =   UserSubjectRelationship::where('user_subject_relationships.user_id',$user->id)
                                                                    ->Join('subjects','subjects.id','=','user_subject_relationships.subject_id')
                                                                    ->select('subjects.subject as userSubject','subjects.id as subjectId')
                                                                    ->get();
            if(!$userSubjectRelationship)
            {
                $userSubjectRelationship = "";
            }

            $userGradeRelationship      =   UserGradeRelationship::where('user_grade_relationships.user_id',$user->id)
                                                                    ->Join('grades','grades.id','=','user_grade_relationships.interestedGrades_id')
                                                                    ->select('grades.grade as userGrade','grades.id as gradeId')
                                                                    ->get();    
            if(!$userGradeRelationship)
            {
                $userGradeRelationship = "";
            }     

            $userReviews                =   UserRatings::WHERE('user_ratings.user_id',$user->id)
                                                            ->join('users','users.id','=','user_ratings.rated_by')
                                                            ->select('user_ratings.*','users.name as userName')
                                                            ->orderBy('updated_at','DESC')
                                                            ->LIMIT(10)->get();
            if(!$userReviews)
            {
                $userReviews = 0;
            }
            $subjects           =   Subject::all();
            
            $grades             =   Grades::all();

            return View('frontend.user.profile')->With([
                'user'                      =>  $user,
                'subjects'                  =>  $subjects,
                'grades'                    =>  $grades,
                'userSubjectRelationship'   =>  $userSubjectRelationship,
                'userGradeRelationship'     =>  $userGradeRelationship,
                'userReviews'               =>  $userReviews
            ]);
        } 
        catch (Exception $e) 
        {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');              
        }
    }

    public function profile($id)
    {
    	try 
        {
            if($id==Session::get('user_id'))
            {
                return redirect ('/user/myProfile');
            }
            $user               		=   User::where('users.id',$id)->where('users.status',1)
			                                    ->Join('grades','grades.id','=','users.myGrade_id')
                                                ->join('roles','roles.id','=','users.role_id')
                                                ->join('user_zipcodes','user_zipcodes.id','=','users.id')
                                                ->select('users.*','user_zipcodes.zipcode','grades.grade as userGrade','grades.id as gradeId','roles.role as userRole')
			                                    ->first();
            if(!$user)
            {
                return redirect('/');
            }

            $userSubjectRelationship    =   UserSubjectRelationship::where('user_subject_relationships.user_id',$user->id)
                                                                    ->Join('subjects','subjects.id','=','user_subject_relationships.subject_id')
                                                                    ->select('subjects.subject as userSubject','subjects.id as subjectId')
                                                                    ->get();

            if(!$userSubjectRelationship)
            {
                $userSubjectRelationship = "";
            }

            $userGradeRelationship      =   UserGradeRelationship::where('user_grade_relationships.user_id',$user->id)
                                                                    ->Join('grades','grades.id','=','user_grade_relationships.interestedGrades_id')
                                                                    ->select('grades.grade as userGrade','grades.id as gradeId')
                                                                    ->get();                                                                    
            if(!$userGradeRelationship)
            {
                $userGradeRelationship = "";
            }
            
            $userReviews                =   UserRatings::WHERE('user_ratings.user_id',$user->id)
                                                            ->join('users','users.id','=','user_ratings.rated_by')
                                                            ->select('user_ratings.*','users.name as userName')
                                                            ->orderBy('updated_at','DESC')
                                                            ->LIMIT(10)->get();

            $subjects                   =   Subject::all();
            
            $grades                     =   Grades::all();

            return View('frontend.user.profile')->With([
                'user'                      =>  $user,
                'subjects'                  =>  $subjects,
                'grades'                    =>  $grades,
                'userSubjectRelationship'   =>  $userSubjectRelationship,
                'userGradeRelationship'     =>  $userGradeRelationship,
                'userReviews'               =>  $userReviews
            ]);
        } 
        catch (Exception $e) 
        {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');              
        }
    }

    public function updateProfile(UserUpdateRequest $request)
    {
        try 
        {
            $user   =   User::where('id',Session::get('user_id'))->where('users.status',1)->first();
            if(!$user)
            {
                return redirect('/');
            }
            $user->Update([
                'name'      =>  $request->name,
                'myGrade_id'=>  $request->myGrade_id,
                'comments'  =>  $request->comments
            ]);

            $zipcode        =   UserZipcode::where('user_id',$user->id)->update([
                'zipcode'   =>  $request->zipcode
            ]);

            if($request->subject_id)
            {
                $userSubRel     =   UserSubjectRelationship::where('user_id',$user->id)->delete();
                foreach ($request->subject_id as $key => $value) 
                {
                    $userSubjectRelationship                =   new UserSubjectRelationship();
                    $userSubjectRelationship->user_id       =   $user->id;
                    $userSubjectRelationship->subject_id    =   $value;
                    $userSubjectRelationship->save();
                }
            }

            if($request->interestedGrades_id)
            {
                $userGradeRel     =   UserGradeRelationship::where('user_id',$user->id)->delete();
                foreach ($request->interestedGrades_id as $key => $value) 
                {
                    $userGradeRelationship                          =   new UserGradeRelationship();
                    $userGradeRelationship->user_id                 =   $user->id;
                    $userGradeRelationship->interestedGrades_id     =   $value;
                    $userGradeRelationship->save();
                }
            }

            $url                =   url('frontend/images/default.jpg');
            $ext                =   '';
            $name               =   '';
            $image              =   '';
            if($request->image)
            {
                if(($request->image->getMimeType() == 'image/png' || $request->image->getMimeType() == 'image/jpeg' || $request->image->getMimeType() == 'image/gif'))
                {
                    
                    $ext            =   $request->image->guessExtension();
                    $name           =   "user_image_".$user->id.".$ext";
                    $url            =   url('frontend/images/'.$name);
                    $image          =   Image::make($request->image)->resize(300,360);
                    if($image) 
                    {
                        $image      ->  save("frontend/images/{$name}");

                        $user->Update([
                            'image' =>  $url
                        ]);
                    }
                    else 
                    {
                        $errors[]   =   'Picture height should not be greater than width.';
                        return redirect()->back()->withErrors($errors);    
                    }
                }
                else 
                {
                    $errors[]       =   'Picture should be a image';
                    return redirect()->back()->withErrors($errors);    
                }
            }

            if($user->role_id == 1)
            {
                if($request->price > 15 || $request->price <= 0)
                {
                    return redirect()->back()->withErrors('Price should be between $0 - $15.')->withInput($request->except('price'));;   
                }
                $user->update([
                    'price' =>  $request->price
                ]);

                $url                =   url('frontend/images/default.jpg');
                $ext                =   '';
                $name               =   '';
                $image              =   '';
                if($request->image)
                {
                    if(($request->image->getMimeType() == 'image/png' || $request->image->getMimeType() == 'image/jpeg' || $request->image->getMimeType() == 'image/gif'))
                    {
                        
                        $ext            =   $request->image->guessExtension();
                        $name           =   "user_image_".$user->id.".$ext";
                        $url            =   url('frontend/images/'.$name);
                        $image          =   Image::make($request->image)->resize(300,360);
                        if($image) 
                        {
                            $image      ->  save("frontend/images/{$name}");

                            $user->Update([
                                'image' =>  $url
                            ]);
                        }
                        else 
                        {
                            $errors[]   =   'Picture height should not be greater than width.';
                            return redirect()->back()->withErrors($errors);    
                        }
                    }
                    else 
                    {
                        $errors[]       =   'Picture should be a image';
                        return redirect()->back()->withErrors($errors);    
                    }
                }

                if($request->resume)
                {
                    /*
                        if(($request->resume->getMimeType() == 'image/png' || $request->resume->getMimeType() == 'image/jpeg' || $request->resume->getMimeType() == 'image/gif'))
                        {
                            
                            $ext            =   $request->resume->guessExtension();
                            $name           =   "user_resume_".$user->id.".$ext";
                            $url            =   url('frontend/images/'.$name);
                            $image          =   Image::make($request->resume)->resize(600,600);
                            if($image) 
                            {
                                $image      ->  save("frontend/images/{$name}");
                                $user->Update([
                                    'resume' => $url
                                ]);
                            }
                            else 
                            {
                                $errors[]   =   'Invalid resume format.';
                                return redirect()->back()->withErrors($errors);    
                            }
                        }
                        else 
                        {
                            $errors[]       =   'Resume should be an Image';
                            return redirect()->back()->withErrors($errors);
                        }
                    */
                    $ext            =   $request->resume->guessExtension();
                    $name           =   "user_resume_".$user->id.".$ext";
                    $url            =   url('frontend/images/'.$name);   
                    $request->resume->move("frontend/images/","$name");
                    $user->Update([
                        'resume' => $url
                    ]);
                }

                if($request->removePhoto == 1)
                {
                    $url                =   url('frontend/images/default.jpg');
                    $user->Update([
                        'image' => $url
                    ]);
                }
                if($request->removeResume == 1)
                {
                    $url                =   url('frontend/images/default.jpg');
                    $user->Update([
                        'resume' => $url
                    ]);
                }
            }


            return redirect('/user/myProfile');
        } 
        catch (Exception $e) 
        {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');    
        }
    }
    
    public function forgetPassword(UserForgetPasswordRequest $request)
    {
        try
        {
            $user           =   User::where('email', $request->email)->where('users.status',1)->first();
            if($user)
            {
                $encrypt    =   md5($user->email.date("d.m.y").date("H:i:s"));
                $string     =   url('/user/resetPassword/'.$encrypt);
                try {                    
                    $forgetpassword                 =   new ResetPasswordLink();
                    $forgetpassword->user_id        =   $user->id;
                    $forgetpassword->unique_key     =   $encrypt;
                    $forgetpassword->save();

                    //$this->sendMail($user->email, 'TeenTutors - Reset Password!', "User Email Id : {$user->email}<br><a href='{$string}'>Click here to reset password. </a>");
                    $this->sendMail($user->parents_email, 'TeenTutors - Reset Password!', "User Email Id : {$user->email}<br><a href='{$string}'>Click here to reset password. </a>");

                //return $string;
                } 
                catch (Exception $e) {
                    return 'Something went wrong. Please try again.';
                }
            }
            else
            {
                return 'Email Id not registered!';
            }
        }
        catch(Exception $e)
        {
            return 'Something went wrong. Please try again.';
        }
    }
    
    public function resetPassword($key)
    {
        try {
            $user_id            =   ResetPasswordLink::where('unique_key',$key)->first();
            if($user_id)
            {
                $expiry_date    =   $user_id->created_at->addHours(720);
                if(Carbon::now()->lte($expiry_date))
                {
                    $user       =   User::where('id',$user_id->user_id)->where('users.status',1)->first();
                    if($user)
                    {
                        $user_id->UPDATE(array(
                            'created_at'    =>  $user_id->created_at->addHours(-720)
                        ));

                        return view('frontend.user.resetPassword')->with([
                            'id'        =>  $user->id
                        ]);
                    }
                    else
                    {
                        return redirect()->back()->withErrors('User not found');
                    }    
                }
                else
                {
                    dd("session-expires");
                }
            }
            else
            {
                return redirect('/');
            }
        } 
        catch (Exception $e) {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');   
        }
    }
    
    public function saveResetPassword(UserResetPasswordRequest $request, $id)
    {
        try {
            $user                =   User::where('id',$id)->where('users.status',1)->first();
            if($user)
            {
                $user           ->  UPDATE(array(
                    'password'  =>  Hash::make($request->password)
                ));
            }
            else
            {
                return 'User not found';
            }            
        } 
        catch (Exception $e) {
            return 'Something went wrong. Please try again.';   
        }
    }
    
    public function tutorRating(Request $request)
    {
        try 
        {                     
            $rating             =   UserRatings::where('rated_by',Session::get('user_id'))->where('user_id',$request->tutorId)->delete();
            
            $rating             =   new UserRatings();
            $rating->rated_by   =   Session::get('user_id');
            $rating->user_id    =   $request->tutorId;
            $rating->rating     =   $request->rating;
            $rating->reviews    =   $request->reviews;
            $rating->save();
            
            $userRating         =   UserRatings::where('user_id',$request->tutorId)->avg('rating');

            $user               =   User::where('id',$request->tutorId)->where('users.status',1)->update([
                "avgRating"     =>  intval($userRating)
            ]);

            return $rating->user_id;
        } 
        catch (Exception $e) 
        {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');         
        }
    }

    public function contact(UserContactRequest $request)
    {
        try 
        {
            $name       =   $request->name;
            $email      =   $request->email;
            $comments   =   $request->comments;
            
            $this->sendMail("gargnim01@gmail.com", "{$name} - {$email}", "{$comments}");
            
            return redirect()->back()->withErrors("Thank you for your feedback.");
        } 
        catch (Exception $e) 
        {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');            
        }

    }
}
