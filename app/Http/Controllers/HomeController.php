<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Session;

use App\Models\User;
use App\Models\UserSubjectRelationship;
use App\Models\UserGradeRelationship;
use App\Models\Subject;
use App\Models\Grades;
use App\Models\UserRatings;
use App\Models\UserZipcode;
use App\Models\ZipcodeLocation;

use Carbon\Carbon;


class HomeController extends Controller
{
    public function isUser()
    {
        if((Session::has('user_id')))
        {
            return true;
        }      
        return false;
    }

    public function calc_distance($point1, $point2)
    {
        $point1  =  ZipcodeLocation::where('zipcode',$point1)->first();
        $point2  =  ZipcodeLocation::where('zipcode',$point2)->first();
        $distance = (3958 * 3.1415926 * sqrt(
                ($point1->latitude - $point2->latitude)
                * ($point1->latitude - $point2->latitude)
                + cos($point1->latitude / 57.29578)
                * cos($point2->latitude / 57.29578)
                * ($point1->longitude - $point2->longitude)
                * ($point1->longitude - $point2->longitude)
            ) / 180);

        return $distance;
    }

    public function getZipcodeByDistance($pZipcode,$pDistance)
    {
        //$pZipcode       =   explode(',', $pZipcode);
        $pZipcode       =   ZipcodeLocation::where('zipcode',$pZipcode)->groupBy('zipcode')->get();
        $rzip           =   [];
        $zipcode        =   UserZipcode::all();

        foreach ($pZipcode as $key => $value) {
            foreach ($zipcode as $key1 => $value1) {
                $distance = $this->calc_distance($value->zipcode,$value1->zipcode);

                if($distance <= $pDistance)
                {
                    $rzip[]     =   $value1->id;
                }
            }
        }
        return $rzip;
    }

    public function home()
    {
    	try 
    	{
            $users          =   User::where('users.role_id',1)
                                ->where('users.status',1)
                                ->orderBy('users.avgRating','DESC')
                                ->limit(4)
                                ->get();

            $subjects       =   Subject::select('id','subject')->get();
            $zipcodes       =   UserZipcode::groupBy('zipcode')->get();

    		return View('frontend.home')->With([
                'users'     =>  $users,
                'zipcodes'  =>  $zipcodes,
                'subjects'  =>  $subjects
            ]);
    	} 
    	catch (Exception $e) 
    	{
    		return redirect()->back()->withErrors('Something went wrong. Please try again.');           	
    	}
    }

    public function aboutUs()
    {
        try 
        {
            return View('frontend.aboutUs');
        } 
        catch (Exception $e) 
        {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');               
        }
    }
        
    public function becomeTutor()
    {
        try 
        {
            return View('frontend.becomeTutor');
        } 
        catch (Exception $e) 
        {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');               
        }
    }

    public function contactUs()
    {
        try 
        {
            return View('frontend.contactUs');
        } 
        catch (Exception $e) 
        {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');               
        }
    }

    public function register()
    {
    	try 
    	{
            if($this->isUser())
            {
                return redirect('/');
            }
            $subjects   =   Subject::select('id','subject')->get();
            $grades     =   Grades::select('id','grade')->get();
    		return View('frontend.user.register')->with([
                "subjects"  =>  $subjects,
                "grades"    =>  $grades
            ]);
    	} 
    	catch (Exception $e) 
    	{
    		return redirect()->back()->withErrors('Something went wrong. Please try again.');           	
    	}
    }

    public function searchTutor()
    {
        try 
        {
            $subjects   =   Subject::select('id','subject')->get();
            $grades     =   Grades::select('id','grade')->get();
            $zipcodes   =   UserZipcode::groupBy('zipcode')->get();
            return View('frontend.searchTutor')->with([
                "subjects"  =>  $subjects,
                "grades"    =>  $grades,
                "zipcodes"  =>  $zipcodes
            ]);
        } 
        catch (Exception $e) 
        {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');            
        }
    }

    public function searchTutorResult(Request $request)
    {
        try 
        {
            $json               =   [];
            $json['result']     =   1;

            $tutorId            =   [];
            $relatedTutorId     =   [];
            
            $subjects           =   [];
            $grades             =   [];
            $zipcodes           =   [];
            $prices             =   [];
            $distance           =   [];
            $tutorRatings       =   [];

            

            $user               =   User::where('role_id',1)->where('users.status',1)->orderBy('avgRating','DESC')->select('id')->get();
            foreach ($user as $key => $value) {
                $relatedTutorId[]   =   $value->id;
            }

            //searching by price
            if(is_numeric(intval($request->minPrice,10)) && is_numeric(intval($request->maxPrice,10))) 
            {
                if(intval($request->minPrice,10) != intval($request->maxPrice,10)){
                    $tutorByPrice           =   User::where('role_id',1)->whereBetween('price', [(intval($request->minPrice,10)), (intval($request->maxPrice,10))])->get();    
                }

                else {
                    $tutorByPrice           =   User::where('role_id',1)->where('price',(intval($request->minPrice,10)))->get();
                }
                
                foreach ($tutorByPrice as $key => $value) {
                    $tutorId[]          =   $value->id;
                    $prices[]           =   $value->id;
                }
            }

            if(count($tutorId)>0){
                $relatedTutorId             =   array_values(array_unique(array_merge($relatedTutorId,$tutorId)));
            }


            //searching by zipcode
            if(!empty($request->zipcode))
            {
                $request->zipcode       =   explode(',', $request->zipcode);
                $tutorByZipcode         =   UserZipcode::where("user_zipcodes.zipcode",$request->zipcode)
                                                        ->join('users','users.id','=','user_zipcodes.user_id')
                                                        ->where('users.role_id',1)->select('users.id')->get();
                foreach ($tutorByZipcode as $key => $value) {
                    $zipcodes[]         =   $value->id;
                }
                $tutorId                =   array_values(array_unique(array_intersect($tutorId,$zipcodes)));   
                if(count($zipcodes)>0)
                {
                    $relatedTutorId     =   array_values(array_unique(array_merge($relatedTutorId,$zipcodes)));
                }         

                //searching by distance
                if($request->distance > 0 && $request->distance < 5)
                {

                    switch ($request->distance) {
                        case 1:
                            $request->distance = 5;
                            break;
                        case 2:
                            $request->distance = 10;
                            break;
                        case 3:
                            $request->distance = 15;
                            break;
                        case 4:
                            $request->distance = 20;
                            break;
                    }

                    $disZipcode             =   [];
                    $disZipcode             =   $this->getZipcodeByDistance($request->zipcode,$request->distance);
                    
                    $tutorByDisZipcode      =   UserZipcode::whereIn("user_zipcodes.id",$disZipcode)
                                                            ->join('users','users.id','=','user_zipcodes.user_id')
                                                            ->where('users.role_id',1)->select('users.id')
                                                            ->get();
                    foreach ($tutorByDisZipcode as $key => $value) {
                        $distance[]         =   $value->id;
                    }

                    $tutorId                =   array_values(array_unique(array_merge($tutorId,$distance)));   

                    if(count($distance)>0)
                    {
                        $relatedTutorId     =   array_values(array_unique(array_merge($relatedTutorId,$distance)));
                    }
                }
            }

            //searching by subject
            if(!empty($request->subject))
            {
                $request->subject       =   explode(',', $request->subject);
                $tutorBySubject         =   UserSubjectRelationship::whereIn("user_subject_relationships.subject_id",$request->subject)
                                                                    ->join('users','users.id','=','user_subject_relationships.user_id')
                                                                    ->where('users.role_id',1)->select('users.id')->get();
                foreach ($tutorBySubject as $key => $value) {
                    $subjects[]         =   $value->id;
                }
                $tutorId                =   array_values(array_unique(array_intersect($tutorId,$subjects)));   
                if(count($subjects)>0)
                {
                    $relatedTutorId     =   array_values(array_unique(array_merge($relatedTutorId,$subjects)));
                }         
            }

            //searching by grade
            if(!empty($request->grade))
            {
                $request->grade         =   explode(',', $request->grade);
                $tutorByGrade           =   User::whereIn("myGrade_id",$request->grade)->where('users.role_id',1)->get();
                foreach ($tutorByGrade as $key => $value) {
                    $grades[]           =   $value->id;
                }
                $tutorId                =   array_values(array_unique(array_intersect($tutorId,$grades)));   
                if(count($grades)>0)
                {
                    $relatedTutorId     =   array_values(array_unique(array_merge($relatedTutorId,$grades)));
                }         
            }

            //searching by tutorRating
            if(!empty($request->tutorRating))
            {
                $request->tutorRating   =   explode(',', $request->tutorRating);
                $tutorByRating          =   User::whereIn("avgRating",$request->tutorRating)->where('users.role_id',1)->get();;
                
                foreach ($tutorByRating as $key => $value) {
                    $tutorRatings[]     =   $value->id;
                }
                $tutorId                =   array_values(array_unique(array_intersect($tutorId,$tutorRatings)));   
                if(count($tutorRatings)>0)
                {
                    $relatedTutorId     =   array_values(array_unique(array_merge($relatedTutorId,$tutorRatings)));
                }         
            }

            $tutors                         =   User::whereIn('users.id',$tutorId)->where('users.role_id',1)->where('users.status',1)
                                                        ->join('grades','grades.id','=','users.myGrade_id')
                                                        ->select('users.*','grades.grade as myGrade')
                                                        ->orderBy('avgRating','DESC')->limit(12)->get();
            if(count($tutors) == 0)
            {
                $tutors                     =   User::whereIn('users.id',$relatedTutorId)->where('users.role_id',1)->where('users.status',1)
                                                        ->join('grades','grades.id','=','users.myGrade_id')
                                                        ->select('users.*','grades.grade as myGrade')
                                                        ->orderBy('avgRating','DESC')->limit(12)->get();

                if(count($tutors) == 0)
                {
                    $tutors                 =   User::where('users.role_id',1)->join('grades','grades.id','=','users.myGrade_id')->where('users.status',1)
                                                        ->select('users.*','grades.grade as myGrade')
                                                        ->orderBy('avgRating','DESC')->limit(12)->get();
                    if(count($tutors) == 0)
                    {
                        $json['noUser']     =   0;
                    }
                }
                $json['result']             =   0;
            }
            
            foreach ($tutors as $key => $value) 
            {
                $userSubjectRelationship    =   UserSubjectRelationship::where('user_subject_relationships.user_id',$value->id)
                                                    ->Join('subjects','subjects.id','=','user_subject_relationships.subject_id')
                                                    ->select('subjects.subject as userSubject')
                                                    ->get();
                $value['subjects']          =   $userSubjectRelationship;
            }
            
            $json['tutors']     =   $tutors;
            return $json;
        } 
        catch (Exception $e) 
        {
            return ('Something went wrong. Please try again.');                  
        }
    }

    public function searchStudent()
    {
        try 
        {
            $subjects   =   Subject::select('id','subject')->get();
            $grades     =   Grades::select('id','grade')->get();
            $zipcodes   =   UserZipcode::groupBy('zipcode')->get();
            return View('frontend.searchStudent')->with([
                "subjects"  =>  $subjects,
                "grades"    =>  $grades,
                "zipcodes"  =>  $zipcodes
            ]);
        } 
        catch (Exception $e) 
        {
            return redirect()->back()->withErrors('Something went wrong. Please try again.');            
        }
    }

    public function searchStudentResult(Request $request)
    {
        try 
        {
            $json               =   [];
            $json['result']     =   1;
            $tutorId            =   [];
            $relatedTutorId     =   [];
            
            $subjects           =   [];
            $grades             =   [];
            $zipcodes           =   [];
            $distance           =   [];

            $user               =   User::where('role_id',2)->where('users.status',1)->select('id')->get();
            foreach ($user as $key => $value) {
                $tutorId[]          =   $value->id;
                $relatedTutorId[]   =   $value->id;
            }

            //searching by zipcode
            if(!empty($request->zipcode))
            {
                $request->zipcode       =   explode(',', $request->zipcode);
                $tutorByZipcode         =   UserZipcode::where("user_zipcodes.zipcode",$request->zipcode)
                                                        ->join('users','users.id','=','user_zipcodes.user_id')
                                                        ->where('users.role_id',2)->select('users.id')->get();
                
                foreach ($tutorByZipcode as $key => $value) {
                    $zipcodes[]         =   $value->id;
                }
                
                $tutorId                =   array_values(array_unique(array_intersect($tutorId,$zipcodes))); 
                
                if(count($zipcodes)>0)
                {
                    $relatedTutorId     =   array_values(array_unique(array_merge($relatedTutorId,$zipcodes)));
                }         

                //searching by distance
                if($request->distance > 0 && $request->distance < 5)
                {

                    switch ($request->distance) {
                        case 1:
                            $request->distance = 5;
                            break;
                        case 2:
                            $request->distance = 10;
                            break;
                        case 3:
                            $request->distance = 15;
                            break;
                        case 4:
                            $request->distance = 20;
                            break;
                    }

                    $disZipcode             =   [];
                    $disZipcode             =   $this->getZipcodeByDistance($request->zipcode,$request->distance);
                    
                    $tutorByDisZipcode      =   UserZipcode::whereIn("user_zipcodes.id",$disZipcode)
                                                            ->join('users','users.id','=','user_zipcodes.user_id')
                                                            ->where('users.role_id',2)->select('users.id')
                                                            ->get();
                    foreach ($tutorByDisZipcode as $key => $value) {
                        $distance[]         =   $value->id;
                    }

                    $tutorId                =   array_values(array_unique(array_merge($tutorId,$distance)));   

                    if(count($distance)>0)
                    {
                        $relatedTutorId     =   array_values(array_unique(array_merge($relatedTutorId,$distance)));
                    }
                }
            }

            //searching by subject
            if(!empty($request->subject))
            {
                $request->subject       =   explode(',', $request->subject);
                $tutorBySubject         =   UserSubjectRelationship::whereIn("user_subject_relationships.subject_id",$request->subject)
                                                                    ->join('users','users.id','=','user_subject_relationships.user_id')
                                                                    ->where('users.role_id',2)->select('users.id')->get();
                foreach ($tutorBySubject as $key => $value) {
                    $subjects[]         =   $value->id;
                }
                $tutorId                =   array_values(array_unique(array_intersect($tutorId,$subjects)));   
                if(count($subjects)>0)
                {
                    $relatedTutorId     =   array_values(array_unique(array_merge($relatedTutorId,$subjects)));
                }         
            }

            //searching by grade
            if(!empty($request->grade))
            {
                $request->grade         =   explode(',', $request->grade);
                $tutorByGrade           =   User::whereIn("myGrade_id",$request->grade)->where('users.role_id',2)->get();
                foreach ($tutorByGrade as $key => $value) {
                    $grades[]           =   $value->id;
                }
                $tutorId                =   array_values(array_unique(array_intersect($tutorId,$grades)));   
                if(count($grades)>0)
                {
                    $relatedTutorId     =   array_values(array_unique(array_merge($relatedTutorId,$grades)));
                }         
            }

            $tutors                         =   User::whereIn('users.id',$tutorId)->where('users.status',1)
                                                        ->join('grades','grades.id','=','users.myGrade_id')
                                                        ->where('users.role_id',2)
                                                        ->select('users.*','grades.grade as myGrade')
                                                        ->limit(12)->get();

            
            if(count($tutors) == 0)
            {
                $tutors                     =   User::whereIn('users.id',$relatedTutorId)->where('users.status',1)
                                                        ->join('grades','grades.id','=','users.myGrade_id')
                                                        ->where('users.role_id',2)
                                                        ->select('users.*','grades.grade as myGrade')
                                                        ->limit(12)->get();
                if(count($tutors) == 0)
                {
                    $tutors                 =   User::where('users.role_id',2)->where('users.status',1)
                                                    ->join('grades','grades.id','=','users.myGrade_id')
                                                    ->select('users.*','grades.grade as myGrade')
                                                    ->limit(12)->get();
                    if(count($tutors) == 0)
                    {
                        $json['noUser']     =   0;
                    }
                }
                $json['result']             =   0;
            }
            
            foreach ($tutors as $key => $value) 
            {
                $userSubjectRelationship    =   UserSubjectRelationship::where('user_subject_relationships.user_id',$value->id)
                                                    ->Join('subjects','subjects.id','=','user_subject_relationships.subject_id')
                                                    ->select('subjects.subject as userSubject')
                                                    ->get();
                $value['subjects']          =   $userSubjectRelationship;
            }

            $json['tutors']     =   $tutors;
            
            return $json;
        } 
        catch (Exception $e) 
        {
            return ('Something went wrong. Please try again.');                  
        }
    }

    public function termsOfService()
    {
        try 
        {
            return View('frontend.termsOfService');   
        } 
        catch (Exception $e) 
        {
           return redirect()->back()->withErrors('Something went wrong. Please try again.');             
        }
    }

    public function privacyPolicy()
    {
        try 
        {
            return View('frontend.privacyPolicy');   
        } 
        catch (Exception $e) 
        {
           return redirect()->back()->withErrors('Something went wrong. Please try again.');             
        }
    }
}
