<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRegistrationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required|string',
            'email'                 => 'required|string',
            'parents_email'         => 'required|string',
            'zipcode'               => 'required|string',
            'password'              => 'required|string',
            'myGrade_id'            => 'required',
            'role_id'               => 'required',
            'subject_id'            => 'required',
            'privacy'               => 'required'
        ];
    }
}
