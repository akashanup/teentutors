<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/'							,	'HomeController@home');
Route::get('/home/aboutUs'				,	'HomeController@aboutUs');
Route::get('/home/contactUs'			,	'HomeController@contactUs');
Route::get('/home/becomeTutor'			,	'HomeController@becomeTutor');
Route::get('/home/register'				,	'HomeController@register');
Route::get('/home/searchTutor'			,	'HomeController@searchTutor');
Route::post('/home/searchTutorResult'	,	'HomeController@searchTutorResult');
Route::get('/home/searchStudent'		,	'HomeController@searchStudent');
Route::post('/home/searchStudentResult'	,	'HomeController@searchStudentResult');
Route::get('/home/termsOfService'		,	'HomeController@termsOfService');
Route::get('/home/privacyPolicy'		,	'HomeController@privacyPolicy');


Route::post('/user/registration'		,	'UserController@registration');
Route::post('/user/login'				,	'UserController@login');
Route::get('/user/logout'	     		,	'UserController@logout');
Route::get('/user/profile/{id}'    		,	'UserController@profile');
Route::get('/user/myProfile'	    	,	'UserController@myProfile');
Route::post('/user/updateProfile'		,	'UserController@updateProfile');
Route::post('/user/tutorRating'			,	'UserController@tutorRating');
Route::post('/user/checkZipcode'		,	'UserController@checkZipcode');
Route::post('/user/addZipcode'			,	'UserController@addZipcode');
Route::post('/user/deleteProfile/{id}'	,	'UserController@deleteProfile');
Route::post('/user/forgetPassword'		,	'UserController@forgetPassword');
Route::get('/user/resetPassword/{key}'	,	'UserController@resetPassword');
Route::post('/user/resetPassword/{id}'	,	'UserController@saveResetPassword');
Route::get('/user/activateAccount/{key}',	'UserController@activateAccount');
Route::get('/user/activate'	    		,	'UserController@activate');
Route::post('/user/contact'				,	'UserController@contact');
