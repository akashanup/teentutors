<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable=['name','image','myGrade_id','comments','avgRating','resume','price','password','status'];
}
