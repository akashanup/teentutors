<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserZipcode extends Model
{
    protected $fillable=['zipcode'];
}
