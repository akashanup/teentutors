<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResetPasswordLink extends Model
{
    protected $fillable=['created_at'];
}
