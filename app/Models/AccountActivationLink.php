<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountActivationLink extends Model
{
    protected $fillable=['created_at'];
}
