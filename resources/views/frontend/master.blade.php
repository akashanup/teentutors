<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="TheTeenTutors - Connects high school tutors with students to provide affordable, high-quality education without any middleman fees.">
        <meta name="author" content="Nimish Garg">
        @yield('meta')
        
        <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('/frontend/assets/ico/favicon/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('/frontend/assets/ico/favicon/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('/frontend/assets/ico/favicon/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/frontend/assets/ico/favicon/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('/frontend/assets/ico/favicon/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('/frontend/assets/ico/favicon/apple-icon-120x120.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('/frontend/assets/ico/favicon/apple-icon-144x144.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('/frontend/assets/ico/favicon/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/frontend/assets/ico/favicon/apple-icon-180x180.png') }}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('/frontend/assets/ico/favicon/android-icon-192x192.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/frontend/assets/ico/favicon/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('/frontend/assets/ico/favicon/favicon-96x96.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/frontend/assets/ico/favicon/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('/frontend/assets/ico/favicon/manifest.json') }}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ asset('/frontend/assets/ico/favicon/ms-icon-144x144.png') }}">
        <meta name="theme-color" content="#ffffff">
        
        
        @yield('title')

        <!-- Bootstrap core CSS -->
        <link href="{{ asset('/frontend/assets/css/bootstrap.css') }}" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="{{ asset('/frontend/assets/css/main.css') }}" rel="stylesheet">
        <link href="{{ asset('/frontend/assets/css/animate-custom.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('/frontend/assets/css/icomoon.css') }}">
        <link href="{{ asset('/frontend/assets/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('/frontend/assets/css/font-awesome.min.css') }}" rel="stylesheet" >
        <link rel="stylesheet" href="{{ asset('/frontend/assets/css/animate.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/frontend/assets/css/jquery.rateyo.min.css') }}" type="text/css"/>
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>

        @yield('styles')

        <script src="{{ asset('/frontend/assets/js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/frontend/assets/js/modernizr.custom.js') }}"></script>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="assets/js/html5shiv.js"></script>
          <script src="assets/js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body data-spy="scroll" data-offset="0" data-target="#navbar-main">

        <div id="navbar-main">
          <!-- Fixed navbar -->
            <div class="navbar navbar-inverse navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" aria-expanded="false" data-toggle="collapse" data-target=".navbar-collapse">
                            <i class="fa fa-bars" aria-hidden="true" style="font-size:25px; color:#3498db;"></i>
                            <!--<span class="icon icon-shield" style="font-size:30px; color:#3498db;">-->
                            <!--mobile-->
                            <!--<img src="{{ asset('/frontend/assets/img/logo.png') }}" alt="Logo" class="img-responsive" height="60px" width="60px">-->
                        </button>
                        <a class="navbar-brand hidden-xs hidden-sm" href="{{url('/')}}" style="margin-top:-12px;margin-left:-25px">
                            <!--<span class="icon icon-shield" style="font-size:18px; color:#3498db;"></span>-->
                            <img src="{{ asset('/frontend/assets/img/logo.png') }}" class="img-responsive" height="25px" width="110px">
                        </a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav pull-left">
                            <li id="home"> <a href="{{url('/')}}" class="smoothScroll"> <b style="color:orange">Home</b></a></li>
                            <li id="aboutUs"> <a href="{{url('/home/aboutUs')}}" class="smoothScroll" style="color:orange"> <b style="color:orange">About Us</b></a></li>
                            <li id="contactUs"> <a href="{{url('/home/contactUs')}}" class="smoothScroll" style="color:orange"> <b style="color:orange">Contact</b></a></li>
                        </ul>
                        <ul class="nav navbar-nav pull-right">
                            @if(Session::has('user_id'))
                                <li id="userBtn"> <a href="{{ url('/user/myProfile') }}" class="smoothScroll"> <b style="color:orange">{{Session::get('user_name')}}</b></a></li>
                                <li id="logoutBtn"> <a href="{{ url('/user/logout') }}" class="smoothScroll"> <b style="color:orange">Logout</b></a></li>
                            @else
                                <li id="loginBtnModal"> <a href="#" class="smoothScroll"> <b style="color:orange">Login</b></a></li>
                                <li id="becomeTutor"> <a href="{{url('/home/becomeTutor')}}" class="smoothScroll"> <b style="color:orange">Become A Tutor</b></a></li>
                            @endif
                        </ul>
                    </div>
                <!--/.nav-collapse -->
                </div>
            </div>
        </div>                                
        
        @yield('contents')

        <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><b style="color:white">&times;</b></button>
                        <h4 class="modal-title">LOGIN</h4>
                    </div>
                    <div class="modal-body">
                        <div id="loginErrorDiv" class="row">
                        </div>
                        <form role="form" id="loginFrm">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <input type="submit" id="loginBtn" class="btn btn-block btn-success" value="Login">
                            </div>
                            <div class="row">
                                <p>
                                    <a href="#" class="btn btn-block" id="lostPasswordBtn">Forget your Password?</a>
                                    Dont Have An Account? <a href="{{ url('/home/register') }}" id="registerBtn">Register Here</a>
                                </p>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="forgetPasswordModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><b style="color:white">&times;</b></button>
                        <h4 class="modal-title">FORGET PASSWORD</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row" id="forgetPasswordErrorDiv" style="font-size:x-small">
                        </div>
                        <div class="row">
                            <form role="form" id="forgetPasswordFrm">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label for="emailForgetUser">Email address</label>
                                    <input type="email" class="form-control" id="emailForgetUser" name="email" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <input type="submit" id="forgetPasswordBtn" class="btn btn-block btn-success" value="Get New Password">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>

        @yield('modals')
        

        <div id="footerwrap">
            <div class="container">
                <!--<h4>Created by <a href="http://blacktie.co">BlackTie.co</a> - Copyright 2014</h4>-->
                <div class="pull-left">
                    <h4>
                        <a href="{{url('/home/privacyPolicy')}}" target='_blank'>Privacy Policy</a> and <a href="{{url('/home/termsOfService')}}" target='_blank'>Terms Of Service</a>
                    </h4>
                </div>
                <div class="pull-right">
                    <h4>
                        Copyright Teen Tutors 2016   
                        <a href="https://www.facebook.com/teentutorsforyou" target="_blank" style="margin-left:25px"> <i class="fa fa-facebook-square" aria-hidden="true"></i> </a>
                        <!--<a href="https://business.google.com/b/106299983305574336976/dashboard/getstarted?ppn=Teen%20Tutors&fvi=gooaFgBHHqYjFJZF3lO039u9oGG0rwoZbrVQYN2C_wEAAAA&hl=en&ppsu=AEf0UR-pSucxfHXADcT-xYurqEzipkZlu-fykwwQME4MWmguzSzhbSZ3RKIewArYSZuZAlIRHEeq0eWaemOKjHOTGWjhq5bJ5A" target="_blank" style="margin-left:20px"> <i class="fa fa-google-plus" aria-hidden="true"></i> </a>-->
                  </h4>
              </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
            

        <script type="text/javascript" src="{{ asset('/frontend/assets/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/frontend/assets/js/retina.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/frontend/assets/js/jquery.easing.1.3.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/frontend/assets/js/smoothscroll.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/frontend/assets/js/jquery-func.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/frontend/assets/js/select2.min.js') }}"></script> 
        <script type="text/javascript" src="{{ asset('/frontend/assets/js/jquery.rateyo.min.js') }}"></script>    
        <script type="text/javascript" src="{{ asset('/frontend/assets/js/myJs.js') }}"></script>     
        <script src=" {{asset('/frontend/assets/js/wow.min.js')}} "></script>
        <script type="text/javascript">
            new WOW().init();
            $(function(){
                $("#loginBtnModal").on("click",function(){
                    $("#loginModal").modal("show");
                });

                $("#lostPasswordBtn").on("click",function(){
                    $("#loginModal").modal("hide");
                    $("#forgetPasswordModal").modal("show");
                });

                $(document).on('click','#loginBtn',function(){
                    $("#loginBtn").prop('disabled', true);
                    var formData = JSON.parse(JSON.stringify(jQuery('#loginFrm').serializeArray()));
                    $.ajax({
                        type    : 'POST',
                        url     : '{{ url("/user/login") }}',
                        data    : formData,
                        complete: function(xhr){
                            if(xhr.status==422)
                            {
                                response = JSON.parse(xhr.responseText);
                                var errors = [];
                                $.each(response, function(i, v) {
                                    $.each(v, function(x, e) {
                                        errors.push(e);
                                    });
                                });
                                var html = displayErrors(errors);
                                $("#loginErrorDiv").html(html);
                                $("#loginBtn").removeAttr('disabled');
                            } 
                        },
                        success : function(data){
                            if(!data)
                            {
                                window.location = '{{ url("/") }}';
                            }
                            else
                            {
                                $("#loginBtn").removeAttr('disabled');
                                var html ="";
                                html    +=  "<div class='errorMessage alert alert-danger'>"                                                     +
                                                "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"  +
                                                data                                                                                            +
                                            "</div>";
                                $("#loginErrorDiv").html(html);    
                            }
                        }          
                    });
                });
                
                $("#loginFrm").on("submit",function(e){
                    e.preventDefault();
                });

                $("#forgetPasswordFrm").on("submit",function(e){
                    e.preventDefault();
                });

                $(document).on('click','#forgetPasswordBtn',function(){
                    $("#forgetPasswordBtn").prop("disabled",true);
                    var formData = JSON.parse(JSON.stringify(jQuery('#forgetPasswordFrm').serializeArray())) ;
                    $.ajax({
                        type    : 'POST',
                        url     : '{{ url("/user/forgetPassword") }}',
                        data    : formData,
                        complete: function(xhr){
                            if(xhr.status==422)
                            {
                                response = JSON.parse(xhr.responseText);
                                var errors = [];
                                $.each(response, function(i, v) {
                                    $.each(v, function(x, e) {
                                        errors.push(e);
                                    });
                                });
                                var html = displayErrors(errors);
                                $("#forgetPasswordErrorDiv").html(html);
                                $("#forgetPasswordBtn").removeAttr('disabled');
                            }

                        },
                        success : function(data){
                            $("#forgetPasswordBtn").removeAttr('disabled');
                            var html="";
                            if(!data)
                            {
                                html = "<div class='row' style='background-color:lightgreen; text-align:center; border-radius:5px;margin-left:10px'>"   +
                                            "<h5>We have sent a password reset link to your and your parent's email id!</h5>"                      +
                                        "</div>";
                                $("#forgetPasswordErrorDiv").html(html);
                                setTimeout(function(){
                                    window.location = '{{ url("/") }}';       
                                }, 3000);    
                            }
                            else
                            {
                                html    =  "<div class='errorMessage alert alert-danger'>"                                                      +
                                                "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"  +
                                                data                                                                                            +
                                            "</div>";
                                $("#forgetPasswordErrorDiv").html(html);    
                            }
                        }          
                    });
                });
            });
        </script>   
        @yield('scripts')

    </body>
</html>
