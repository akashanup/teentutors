@extends('frontend.master')

@section('title')
    <title>Register</title>
@stop

@section('contents')
    <div class="container" style="margin-bottom:50px;margin-top:50px">
        <br>
        @if(count($errors))
            @foreach($errors->all() as $error)
            <div class="row alert alert-danger" id="errorDiv">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{$error}}
            </div>
            @endforeach
        @endif
    </div>    
    <!-- ==== SERVICES ==== -->
        <div class="container" id="services" name="services" style="margin-bottom:55px">
            <div class="row" style="margin-top:-50px">
                <h1 class="centered">REGISTRATION</h1>
                <hr>
                <div class="col-lg-offset-2 col-lg-8">
                    <form id="signUpFrm" method="Post" action="{{ url('/user/registration') }}" enctype='multipart/form-data'>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" id="subjects" value="{{ $subjects }}">
                        <input type="hidden" id="grades" value="{{ $grades }}">
                        <input type="hidden" id="latitude" name="latitude">
                        <input type="hidden" id="longitude" name="longitude">
                        <div class="row">
                            <div class="form-group">
                                <label for="name">Name (<i class='fa fa-star' style="color:red;font-size:xx-small"></i>)</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{old('name')}}">
                            </div>
                            <div class="form-group">
                                <label for="email">Email (<i class='fa fa-star' style="color:red;font-size:xx-small"></i>)</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{old('email')}}">
                            </div>
                            <div class="form-group">
                                <label for="parent_email">Parent's Email (<i class='fa fa-star' style="color:red;font-size:xx-small"></i>)</label>
                                <input type="email" class="form-control" id="parents_email" name="parents_email" value="{{old('parents_email')}}" placeholder="Parent's Email">
                            </div>
                            <div class="form-group">
                                <label for="zipcode">Zip Code (<i class='fa fa-star' style="color:red;font-size:xx-small"></i>)</label>
                                <input type="text" class="form-control" id="zipcode" name="zipcode" placeholder="Zip Code" value="{{old('zipcode')}}">
                            </div>
                            <div class="form-group">
                                <label for="password">Password (<i class='fa fa-star' style="color:red;font-size:xx-small"></i>) <a href="#" id="passwordPolicy">Password Policy</a></label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="">
                            </div>
                            <div class="form-group">
                                <label for="cnf_password">Confirm Password (<i class='fa fa-star' style="color:red;font-size:xx-small"></i>)</label>
                                <input type="password" class="form-control" id="cnf_password" name="cnf_password" placeholder="Confirm Password" value="">
                            </div>
                            <div class="form-group">
                                <label class="radio-inline control-label">
                                    <input type="radio" class="radioBtn" name="role_id" value="1"><b>I am a Tutor</b>
                                </label>
                                <label class="radio-inline control-label">
                                    <input type="radio" class="radioBtn" name="role_id" value="2"><b>I am a Student</b>
                                </label>
                            </div>
                        </div>
                        <div class="row" id="userType">
                        </div>
                    </form>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="row">
                <br>
                <br>
            </div>
        </div>
    <!-- SERVICES -->    
@stop

@section('modals')
    <div class="modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><b style="color:white">&times;</b></button>
                    <h4 class="modal-title">Privacy Policy And Terms Of Use.</h4>
                </div>
                <div class="modal-body">
                    <div class="row" id="rateErrorDiv">
                    </div>
                    <div class="row" id="rateBodyDiv">
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="passwordPrivacyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><b style="color:white">&times;</b></button>
                    <h4 class="modal-title">Password Policy.</h4>
                </div>
                <div class="modal-body">
                    <div class="row" id="rateErrorDiv">
                    </div>
                    <div class="row" id="rateBodyDiv">
                        <span>Password must be of atleast 8 characters with at least one uppercase character and one digit.</span>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        $(function(){
            $("#registerBtn").addClass("active");

            $(".select").select2();

            $("#loginBtn").on("click",function(){
                $("#loginModal").modal("show");
            });

            $(document).on("click",".radioBtn",function(){
                var subjects    =   $.parseJSON($("#subjects").val());
                var grades      =   $.parseJSON($("#grades").val());
                var html        =   "";
                                    

                if($(this).val() == 1)
                {
                    html += 
                            "<div class='form-group'>"                                                                      +
                                "<label for='image'>Photo</label>"                                                          +
                                "<input type='file' class='form-control' id='image' name='image' placeholder='image'>"      +
                            "</div>"                                                                                        +
                            "<div class='form-group'>"                                                                      +
                                "<label for='resume'>Resume</label>"                                                        +   
                                "<input type='file' class='form-control' id='resume' name='resume' placeholder='resume'>"   +
                            "</div>"                                                                                        +
                            "<div class='form-group'>"                                                                      +
                                "</select>"                                                                                 +
                                "<label for='myGrade_id'>Grade (<i class='fa fa-star' style='color:red;font-size:xx-small'></i>)</label>" +
                                "<select name='myGrade_id' id='myGrade_id' class='select form-control'>";
                                    $.each(grades,function(i,v){
                                        if(i>9){
                                            html    +=  "<option value='"+v.id+"'>"+v.grade+"</option>";
                                        }
                                    });
                                html += "</select>"                                                                         +
                            "</div>"                                                                                        +
                            "<div class='form-group'>"                                                                      +
                                "<label for='subject_id'>Subjects You Want To Teach (<i class='fa fa-star' style='color:red;font-size:xx-small'></i>)</label>" +
                                "<select name='subject_id[]' id='subject_id' class='select form-control' multiple='multiple'>";
                                    $.each(subjects,function(i,v){
                                        html    +=  "<option value='"+v.id+"'>"+v.subject+"</option>";
                                    });
                                html += "</select>"                                                                         +
                            "</div>"                                                                                        +
                            "<div class='form-group'>"                                                                      +
                                "<div class='row'>"                                                                         +
                                    "<label style='margin-left:15px'>Grades You are Interested in Teaching (<i class='fa fa-star' style='color:red;font-size:xx-small'></i>)</label><br>"    +
                                    "<div class='col-lg-5 col-md-5 col-sm-5 col-xs-5'>"                                                                     +
                                        "<label for='from'>From</label>"                                                                                    +
                                        "<select name='from' id='from' class='select form-control'>";
                                            $.each(grades,function(i,v){
                                                html    +=  "<option value='"+v.id+"'>"+v.grade+"</option>";
                                            });
                                        html += "</select>"                                                                                                 +
                                    "</div>"                                                                                                                +
                                    "<div class='col-lg-1 col-md-1 col-sm-1 col-xs-1'>"                                                                     +
                                        "<label>&nbsp;</label>"                                                                                             +
                                        "<span><b>-</b></span>"                                                                                             +
                                    "</div>"                                                                                                                +
                                    "<div class='col-lg-5 col-md-5 col-sm-5 col-xs-5'>"                                                                     +
                                        "<label for='to'>To</label>"                                                                                        +
                                        "<select name='to' id='to' class='select form-control'>";
                                            $.each(grades,function(i,v){
                                                if(i>0){
                                                    html    +=  "<option value='"+v.id+"'>"+v.grade+"</option>";
                                                }
                                            });
                                        html += "</select>"                                                                                                 +
                                    "</div>"                                                                                                                +
                                "</div>"                                                                                                                    +
                                /*"<label for='interestedGrades'>Grades You Are Interested To Teach (<i class='fa fa-star' style='color:red;font-size:xx-small'></i>)</label>" +
                                "<select name='interestedGrades_id[]' id='interestedGrades' class='select form-control' multiple='multiple'>";
                                    $.each(grades,function(i,v){
                                        html    +=  "<option value='"+v.id+"'>"+v.grade+"</option>";
                                    });
                                html += "</select>"                                                                         +*/
                            "</div>"                                                                                        +
                            "<div class='form-group'>"                                                                      +
                                "<label for='price'>Price ($0 - $15) (<i class='fa fa-star' style='color:red;font-size:xx-small'></i>)</label>" +
                                "<input type='number' min='0' max='15' name='price' id='price' class='form-control' value='{{old('price')}}''>"                +
                            "</div>";
                }
                
                else if($(this).val() == 2)
                {
                    html += 
                            "<div class='form-group'>"                                                                      +
                                "</select>"                                                                                 +
                                "<label for='myGrade_id'>Grade (<i class='fa fa-star' style='color:red;font-size:xx-small'></i>)</label>" +
                                "<select name='myGrade_id' id='myGrade_id' class='select form-control'>";
                                    $.each(grades,function(i,v){
                                        html    +=  "<option value='"+v.id+"'>"+v.grade+"</option>";
                                    });
                                html += "</select>"                                                                         +
                            "</div>"                                                                                        +
                            "<div class='form-group'>"                                                                      +
                                "<label for='subject_id'>Subjects You Want To Study (<i class='fa fa-star' style='color:red;font-size:xx-small'></i>)</label>" +
                                "<select name='subject_id[]' id='subject_id' class='select form-control' multiple='multiple'>";
                                    $.each(subjects,function(i,v){
                                        html    +=  "<option value='"+v.id+"'>"+v.subject+"</option>";
                                    });
                                html += "</select>"                                                                         +
                            "</div>"                                                                                        +
                            "<div class='form-group'>"                                                                      +
                                "<label for='interestedGrades'>Grade Tutor Should Be In (<i class='fa fa-star' style='color:red;font-size:xx-small'></i>)</label>" +
                                "<select name='interestedGrades_id[]' id='interestedGrades' class='select form-control' multiple='multiple'>";
                                    $.each(grades,function(i,v){
                                        if(i>9)
                                        {
                                            html    +=  "<option value='"+v.id+"'>"+v.grade+"</option>";
                                        }
                                    });
                                html += "</select>"                                                                         +
                            "</div>";
                }

                html    +=  "<div class='form-group'>"                                                                      +
                                "<label for='comments'>Comments</label>"                                                    +
                                "<textarea id='comments' cols='30' rows='3' class='form-control' name='comments'>"          +
                                "{{old('comments')}}"                                                                       +
                                "</textarea>"                                                                               +
                            "</div>"                                                                                        +
                            "<div class='form-group'>"                                                                      +
                                "<input type='checkbox' value='1' name='privacy'>"                                          +
                                "I agree to <a href='{{url('/home/privacyPolicy')}}' target='_blank'>privacy policy</a> and <a href='{{url('/home/termsOfService')}}' target='_blank'>terms of service.(<i class='fa fa-star' style='color:red;font-size:xx-small'></i>)</a>"                 +
                            "</div>"                                                                                        +
                            "<div class='form-group' style='font-size:small'>"                                              +
                                "DISCLAIMER: Besides your password and parent’s email, all the information that you provide will be made public."+
                            "</div>"                                                                                        +

                            "<div class='form-group'>"                                                                      +
                                "<input type='button' id='signUpBtn' class='btn btn-block btn-success' value='Sign Up'>"    +
                            "</div>";     
                $("#userType").html(html); 
                $(".select").select2();                           
            });
            
            /*$(document).on("click","#privacyText",function(){
                $("#privacyModal").modal("show");
            });*/

            $(document).on("click","#passwordPolicy",function(){
                $("#passwordPrivacyModal").modal("show");
            });
            
            $(document).on("click","#signUpBtn",function(){
                $("#signUpBtn").prop("disabled",true);
                var zipcode =   $("#zipcode").val();
                if(zipcode)
                {
                    $.ajax({
                        url : "https://maps.googleapis.com/maps/api/geocode/json?components=postal_code:"+zipcode+"&sensor=false",
                        method: "POST",
                        complete: function(xhr){
                            if(xhr.status==422)
                            {
                                response = JSON.parse(xhr.responseText);
                                var errors = [];
                                $.each(response, function(i, v) {
                                    $.each(v, function(x, e) {
                                        errors.push(e);
                                    });
                                });
                                var html = displayErrors(errors);
                                console.log(html);
                                $("#errorDiv").append(html);

                            } 
                        },
                        success:function(data){
                           console.log(data.results[0]);
                           if(data.results[0] == undefined)
                           {
                                alert("Invalid Zipcode");
                                $("#signUpBtn").removeAttr('disabled');
                           } 
                           else
                           {
                               latitude = data.results[0].geometry.location.lat;
                               longitude= data.results[0].geometry.location.lng;
                               //alert("Lat = "+latitude+"- Long = "+longitude);
                                $("#latitude").val(latitude.toString());
                                $("#longitude").val(longitude.toString());
                                
                                $("#signUpFrm").submit();
                            }
                        },
                    });
                }
                else
                {
                    $("#signUpFrm").submit();
                }
                //$("#signUpFrm").submit();
            });
                
        });
    </script>
@stop