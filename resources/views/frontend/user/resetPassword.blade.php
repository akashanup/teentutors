@extends('frontend.master')

@section('title')
<title>Reset Password</title>
@stop

@section('contents')
    <div class="container" style="margin-bottom:50px;margin-top:50px">
        <br>
        @if(count($errors))
            @foreach($errors->all() as $error)
            <div class="row alert alert-danger" id="errorDiv">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{$error}}
            </div>
            @endforeach
        @endif
    </div>

    <div class="container">
        <div class="row" style="text-align:center">
            <h1>Reset Password</h1>
        </div>
        <div class="row" id="resetPasswordErrorDiv">
        </div>
        <div class="row" style="margin-top:50px;margin-bottom:200px ">
            <form role="form" id="resetPasswordFrm">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group login-password col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
                    <div>
                        <input name="password" id="resetPassword" class="form-control input" size="20" placeholder="Enter New Password" type="password">
                    </div>
                </div>
                <div>
                    <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12 col-xs-12">
                        <input id="resetPasswordBtn" class="btn  btn-block btn-lg btn-primary" value="Reset Password" type="button">
                    </div>
                    <br>
                    <br>
                </div>
            </form>
        </div>
    </div>
@stop


@section('scripts')
	<script type="text/javascript">
        
        $(function () {
            $(document).on('click','#resetPasswordBtn',function(){
                var formData = JSON.parse(JSON.stringify(jQuery('#resetPasswordFrm').serializeArray())) ;
                $.ajax({
                    type    : 'POST',
                    url     : '{{ url("/user/resetPassword/".$id) }}',
                    data    : formData,
                    complete: function(xhr){
                        if(xhr.status==422)
                        {
                            response = JSON.parse(xhr.responseText);
                            var errors = [];
                            $.each(response, function(i, v) {
                                $.each(v, function(x, e) {
                                    errors.push(e);
                                });
                            });
                            var html = displayErrors(errors);
                            $("#resetPasswordErrorDiv").html(html);
                        } 
                    },
                    success : function(data){
                        var html="";
                        if(!data)
                        {
                            html = "<div class='col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-12 col-xs-12' style='background-color:lightgreen;text-align:center; border-radius:10px'>"      +
                                        "<h5 style='margin-top:10px'><b>Password Reset Successful!</b></h5>"                                                                                            +
                                    "</div>";
                            $("#resetPasswordErrorDiv").html(html);
                            setTimeout(function(){
                                window.location = '{{ url("/") }}';       
                            }, 1200);    
                        }
                        else
                        {
                            html    =  "<div class='errorMessage alert alert-danger'>"                                                      +
                                            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"  +
                                            data                                                                                            +
                                        "</div>";
                            $("#resetPasswordErrorDiv").html(html);    
                        }
                    }          
                });
            });
        });
    </script>
@stop


        