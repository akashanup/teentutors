@extends('frontend.master')

@section('title')
    <title>{{$user->name}}</title>
@stop

@section('contents')
     
    <!-- ==== GREYWRAP ==== -->
        <div id="greywrap">
            <div class="container" style="margin-top:5px">
                @if(count($errors))
                    <br>
                    @foreach($errors->all() as $error)
                    <div class="row alert alert-danger" id="errorDiv">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{$error}}
                    </div>
                    @endforeach
                @endif
            </div>
            <div class="container" style="margin-top:15px">  
                <div class="row">
                    <h1>{{$user->userRole}}</h1>
                    <br>
                    @if($user->id == Session::get('user_id'))
                        <div class="row">
                            <div class="pull-right">
                                <a class="btn btn-danger btn-block" data-id="{{Session::get('user_id')}}" id="deleteUserProfileBtn">Delete Account</a>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="container">
                <div class="row">
                    @if($user->role_id == 1)
                        <div class="col-lg-3 centered">
                            <div class="row">
                                <img class="img-responsive" src="{{$user->image}}" height="300px" width="360px">
                            </div>
                            @if($user->role_id == 1)
                                <div class="row">
                                    <br>
                                    <a href="{{$user->resume}}" target="_blank" id="btnResume" class="btn btn-block btn-primary">View Resume</a>
                                </div>
                            @endif
                            @if((Session::has('user_id')) && (($user->role_id == 1) && (Session::get('user_role') == 2)))
                                <div class="row">
                                    <br>
                                    <a id="rateReviewBtn" class="btn btn-block btn-warning" data-id="{{$user->id}}">Rate/Review Tutor</a>
                                </div>
                            @endif
                        </div>
                    @else
                        <div class="col-lg-1 centered">
                        </div>
                    @endif
                    <div class="col-lg-8 col-lg-offset-1">
                        <div class="row">
                            <table class="table table-hover table-responsive"  style="text-align:left">
                                <tr>
                                    <th>Name</th>
                                    <td>
                                        {{$user->name}}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>
                                        {{$user->email}}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Grade</th>
                                    <td>
                                        {{$user->userGrade}}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Subject(s)</th>
                                    <td>
                                            @foreach($userSubjectRelationship as $u)
                                                <i class="fa fa-circle" style="font-size:65%; color:black" aria-hidden="true"></i> {{$u->userSubject}}<br>
                                            @endforeach
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <th>ZipCode</th>
                                    <td>
                                        {{$user->zipcode}}
                                    </td>
                                </tr>
                                @if($user->role_id == 1)
                                    <tr>
                                        <th>Interested Student's Grade(s)</th>
                                        <td>
                                            @foreach($userGradeRelationship as $u)
                                                <i class="fa fa-circle" style="font-size:65%; color:black" aria-hidden="true"></i> {{$u->userGrade}}<br>
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Price</th>
                                        <td>${{$user->price}}</td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Rating
                                        </th>
                                        <td>
                                            <div class="row">
                                                <div class="row">
                                                    <input type="hidden" id="userRating" value="{{$user->avgRating}}">
                                                    <div id="rateYos">
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @elseif($user->role_id == 2)
                                    <tr>
                                        <th>Interested Tutor's Grade(s)</th>
                                        <td>
                                            @foreach($userGradeRelationship as $u)
                                                <i class="fa fa-circle" style="font-size:65%; color:black" aria-hidden="true"></i> {{$u->userGrade}}<br>
                                            @endforeach
                                        </td>
                                    </tr>
                                @endif
                                <tr>
                                    <th>Comments</th>
                                    <td>
                                        {{$user->comments}}
                                    </td>
                                </tr>
                            </table>
                        </div>
                        @if($user->id == Session::get('user_id'))
                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <a class="btn btn-success btn-block" id="editUserProfileBtn">Edit Profile</a>
                                </div>
                            </div>
                        @endif

                        <div class="row">
                            @if($user->role_id == 1)
                                <br><h2>Reviews</h2><hr>
                                <div class="row">
                                    @foreach($userReviews as $key=>$review)
                                        @if($review->reviews)
                                            <div class="row" style="background-color:#F5F5F5">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align:left">
                                                    <b>{{$review->userName}}</b>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-lg-offset-2 col-md-offset-2" style="text-align:right">
                                                    {{$review->updated_at->toFormattedDateString()}}
                                                </div>
                                            </div>
                                            <div class="row" style="background-color:#F5F5F5">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:left">
                                                    <span>{{$review->reviews}}</span>
                                                </div>

                                                <div class="col-lg-3 col-md-3 col-sm-9 col-xs-9 col-lg-offset-8 col-md-offset-8 col-sm-offset-2 col-xs-offset-2">
                                                    <input type="hidden" value="{{$review->rating}}" id="rateVal{{$key}}">
                                                    <div id="rateDiv{{$key}}" class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                        @endif
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>   
                </div>
            </div>
            <br>
            @if($user->role_id == 2)
            <br><br><br><br>
            @endif
        </div>
    <!-- Greywrap -->  
@stop

@section('modals')
    <div class="modal fade" id="editUserProfile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><b style="color:white">&times;</b></button>
                    <h4 class="modal-title">EDIT PROFILE</h4>
                </div>
                <div class="modal-body">
                    <div class="row" id="editUserErrorDiv">
                    </div>
                    <div class="row" id="editUserBodyDiv">
                        <form id="editProfileFrm" enctype='multipart/form-data' method='POST' action='{{ url("/user/updateProfile")}}'>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{$user->name}}">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <label for="myGrade_id">Grade</label><br>
                                        <select name="myGrade_id" id="myGrade_id" class="form-control">
                                            @if($user->role_id == 1)
                                                @foreach($grades as $key=>$grade)
                                                    @if($key>9)
                                                        @if($user->myGrade_id == $grade->id)
                                                            <option value="{{$grade->id}}" selected>{{$grade->grade}}</option>
                                                        @else
                                                            <option value="{{$grade->id}}">{{$grade->grade}}</option>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @else
                                                @foreach($grades as $grade)
                                                    @if($user->myGrade_id == $grade->id)
                                                        <option value="{{$grade->id}}" selected>{{$grade->grade}}</option>
                                                    @else
                                                        <option value="{{$grade->id}}">{{$grade->grade}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>        
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <label for="zipcode">Zip Code</label>
                                        <input type="text" class="form-control" id="zipcode" name="zipcode" placeholder="Zip Code" value="{{$user->zipcode}}">
                                    </div>
                                </div>                                
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <label for="interestedGrades_id">Interested Grade(s)</label><br>
                                        <select name="interestedGrades_id[]" id="interestedGrades_id" class="form-control" multiple="multiple">
                                            @foreach($grades as $grade)
                                                <?php $flag = 0;?>
                                                @foreach($userGradeRelationship as $u)
                                                    @if($u->gradeId == $grade->id)
                                                        <option value="{{$grade->id}}" selected>
                                                            {{$grade->grade}}
                                                        </option>
                                                        <?php $flag = 1;?>
                                                    @endif
                                                @endforeach
                                                @if($flag == 0)
                                                    <option value="{{$grade->id}}">                                                    
                                                        {{$grade->grade}}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>        
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <label for="subject_id">Subject(s)</label><br>
                                        <select name="subject_id[]" id="subject_id" class="form-control" multiple="multiple">
                                            @foreach($subjects as $subject)
                                                <?php $flag = 0;?>
                                                @foreach($userSubjectRelationship as $u)
                                                    @if($u->subjectId == $subject->id)
                                                        <option value="{{$subject->id}}" selected>
                                                            {{$subject->subject}}
                                                        </option>
                                                        <?php $flag = 1;?>
                                                    @endif
                                                @endforeach
                                                @if($flag == 0)
                                                    <option value="{{$subject->id}}">                                                    
                                                        {{$subject->subject}}
                                                    </option>
                                                @endif
                                            @endforeach
                                        </select>        
                                    </div>
                                </div>
                            </div>

                            @if($user->role_id == 1)
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <label for="image">Photo</label>
                                            <input type="file" class="form-control" id="image" name="image" placeholder="Photo">
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <label for="resume">Resume</label>
                                            <input type="file" class="form-control" id="resume" name="resume" placeholder="Resume">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <input type="checkbox" name="removePhoto" value="1">Remove Photo
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <input type="checkbox" name="removeResume" value="1">Remove Resume
                                        </div>
                                    </div>
                                </div>
                                <div class='form-group'>
                                    <label for='price'>Price(in $)</label>
                                    <input type='number' min='0' max='15' name='price' id='price' value="{{$user->price}}" class='form-control'>
                                </div>
                            @endif

                            <div class="form-group">
                                <label for="comments">Comments</label>
                                <textarea id='comments' cols='30' rows='3' class='form-control' name='comments'>{{$user->comments}}
                                </textarea>
                            </div>
                            <div class="form-group"><br>
                                <input type="submit" id="updateBtn" class="form-control btn btn-block btn-success" value="Update">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>    

    <div class="modal fade" id="modal-review" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><b style="color:white">&times;</b></button>
                    <h4 class="modal-title">RATE/REVIEW</h4>
                </div>
                <div class="modal-body">
                    <div class="row" id="rateErrorDiv">
                    </div>
                    <div class="row" id="rateBodyDiv">
                        <form  role="form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="tutorId" id="tutorId">
                            <div class="form-group">
                                <label for="rateYoModal">Rate</label>
                                <div id="rateYoModal"></div>
                                <input type="hidden" name="tutorRating" id="tutorRating">
                            </div>
                            <div class="form-group">
                                <label for="reviews">Review</label>
                                <textarea id='reviews' cols='30' rows='3' class='form-control' name='reviews'></textarea>
                            </div>
                            <input type="button" id="btnReview" class="btn btn-block btn-success" value="Submit Review">
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <!--<div class="modal fade" id="modal-resume" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><b style="color:white">&times;</b></button>
                    <h4 class="modal-title">RESUME</h4>
                </div>
                <div class="modal-body">
                    <div class="row" id="resumeErrorDiv">
                    </div>
                    <div class="row" id="reaumeBodyDiv">
                        <img src="" height="575px" width="575px" class="img-responsive">
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>--> 
@stop

@section('scripts')
    <script type="text/javascript">
        $(function(){
            $("#userBtn").addClass("active");

            $("#deleteUserProfileBtn").on("click",function(){
                if(window.confirm("Click OK to delete this account"))
                {
                    var formData            =   {'_token':'{{csrf_token()}}'};
                    id                      =   $(this).attr("data-id");
                    $.ajax({
                        type    : 'POST',
                        url     : '{{ url("/user/deleteProfile") }}/'+id,
                        data    : formData,
                        complete: function(xhr){
                            if(xhr.status==422)
                            {
                                response = JSON.parse(xhr.responseText);
                                var errors = [];
                                $.each(response, function(i, v) {
                                    $.each(v, function(x, e) {
                                        errors.push(e);
                                    });
                                });
                                var html = displayErrors(errors);
                                $("#rateErrorDiv").append(html);
                            } 
                        },
                        success : function(data){
                            window.location = "{{url('/')}}";
                        }          
                    });
                }
            });

            $("#editUserProfileBtn").on("click",function(){
                $("#editUserProfile").modal("show");
            });

            $("select").select2();

            $("#rateReviewBtn").on("click",function(){
                var userId     =   $(this).attr('data-id');
                $("#tutorId").val(userId);
                $("#modal-review").modal("show");
            });

            $("#modal-review").on('show.bs.modal', function () {
                var $rateYo = $("#rateYoModal").rateYo();
                $rateYo.rateYo("destroy");   
                $("#review").attr("value",""); 
            });

            $("#modal-review").on('shown.bs.modal', function () {
                var $rateYo = $("#rateYo").rateYo();
                $rateYo.rateYo("destroy");    
                $("#rateYoModal").rateYo({
                    fullStar : true,
                    onSet: function (rating, rateYoInstance) {
                        //alert("Rating is set to: " + rating);
                        $("#tutorRating").val(rating);
                    }
                });
            });

            $("#btnReview").on("click",function(){
                var formData            =   {'_token':'{{csrf_token()}}'};
                formData.tutorId        =   $("#tutorId").val();
                formData.rating         =   parseInt($("#tutorRating").val());
                formData.reviews        =   $("#reviews").val();
                $.ajax({
                    type    : 'POST',
                    url     : '{{ url("/user/tutorRating") }}',
                    data    : formData,
                    complete: function(xhr){
                        if(xhr.status==422)
                        {
                            response = JSON.parse(xhr.responseText);
                            var errors = [];
                            $.each(response, function(i, v) {
                                $.each(v, function(x, e) {
                                    errors.push(e);
                                });
                            });
                            var html = displayErrors(errors);
                            $("#rateErrorDiv").append(html);
                        } 
                    },
                    success : function(data){
                        window.location = "{{url('/user/profile').'/'}}"+data;
                    }          
                });
            });

            $("#rateYos").rateYo({
                rating: $("#userRating").val(),
                precision: 1,
                readOnly: true,
                fullStar : true
            });

            /*$("#btnResume").on("click",function(){
                $("#modal-resume").modal("show");
            });*/

            for(var i = 0; i < 10; i++)
            {
                var ratingValue     =   $("#rateVal"+i).val();
                $("#rateDiv"+i).rateYo({
                    rating: ratingValue,
                    precision: 1,
                    readOnly: true
                });                
            }


        });
    </script>
@stop