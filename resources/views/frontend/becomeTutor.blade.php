@extends('frontend.master')

@section('title')
    <title>Tutor</title>
@stop

@section('contents')
    <div class="container" style="margin-bottom:50px;margin-top:50px">
        <br>
        @if(count($errors))
            @foreach($errors->all() as $error)
            <div class="row alert alert-danger" id="errorDiv">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{$error}}
            </div>
            @endforeach
        @endif
    </div>

    <!-- ==== SECTION DIVIDER2 -->
        <section class="section-divider textdivider divider2" style="margin-top:-71px;">
        </section>
    <!-- section divider2 -->

    <div class="container" id="contact" name="contact">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2" style="font-size:x-large">
                <h1 class="centered">EARN WHILE YOU LEARN</h1>
                <hr>
                <ul>
                    <li>Work at your own pace, at your own time, with your own rule.</li>
                    <li>Charge up to $15 with no registration/website fee, much more then minimum wage.</li>
                    <li>Teach what you love.</li>
                    <li>Build up your resume as a responsible, valuable, and independent individual.</li>
                    <li>What else do you want? Join us now.</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <br>
            <div class="col-lg-4 col-lg-offset-4">
                <a href="{{url('/home/register')}}" class="btn btn-block btn-success">Register Here</a>
            </div> 
            <br>
            <br>
            <br>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        $(function(){
            $("#becomeTutor").addClass("active");
            
        });
    </script>
@stop