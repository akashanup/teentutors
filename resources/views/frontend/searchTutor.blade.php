@extends('frontend.master')

@section('title')
    <title>Tutor</title>
@stop

@section('contents')
    <div class="container" style="margin-bottom:50px;margin-top:50px">
        <br>
        @if(count($errors))
            @foreach($errors->all() as $error)
            <div class="row alert alert-danger" id="errorDiv">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{$error}}
            </div>
            @endforeach
        @endif
    </div>
    <div class="container" style="margin-bottom:75px">
        <div class="row" style="margin-top:-60px">
            <h2>Search Here</h2>
            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6">
                <div class="form-group">
                    <label for="subject_id">Subject(s)</label>
                    <select name="subject_id" id="subject_id" class="select form-control" multiple="multiple">
                        @foreach($subjects as $subject)
                                <option value="{{$subject->id}}">{{$subject->subject}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6">
                <div class="form-group">
                    <label for="zipcode">Zipcode</label>
                    <input type="text" class="form-control" id="zipcode">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="searchSubject1">&nbsp;</label>
                    <button class="btn btn-block btn-success searchSubject" type="button" id="searchSubject1">Search</button>
                </div>
            </div>
        </div>
        <div class="row">
            <br><br>
            <div class="col-lg-2">
                <form id="searchFrm">
                    <div class="row">
                        <h4><b>Advanced Filters</b></h4>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label for="myGrade_id">Grade(s) Of Tutor</label>
                            <select name="myGrade_id" id="myGrade_id" class="select form-control" multiple="multiple">
                                @foreach($grades as $grade)
                                    @if($grade->id > 10)
                                        <option value="{{$grade->id}}">{{$grade->grade}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="distance">Distance</label>
                            <select name="distance" id="distance" class="select form-control">
                                <option value="0">None</option>
                                <option value="1">Up to 5 Miles</option>
                                <option value="2">Up to 10 Miles</option>
                                <option value="3">Up to 15 Miles</option>
                                <option value="4">Up to 20 Miles</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="price">Price Range</label>
                            <div class="row">
                                <div class="col-lg-6">
                                    <input type="number" id="minPrice" min="0" max="14" class="form-control">
                                </div>
                                <div class="col-lg-6">
                                    <input type="number" id="maxPrice" min="1" max="15" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="rateYo">Tutor Rating</label>
                            <div id="rateYo">
                            </div>
                            <input type="number" style="display:none" id="tutorRating" value="0">
                        </div>
                        <div class="form-group">
                            <input type="checkbox" id="5stars"><span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> 5 STARS</span><br>
                            <input type="checkbox" id="4stars"><span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> 4 STARS</span><br>
                            <input type="checkbox" id="3stars"><span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> 3 STARS</span><br>
                            <input type="checkbox" id="2stars"><span> <i class="fa fa-star"></i> <i class="fa fa-star"></i> 2 STARS</span><br>
                            <input type="checkbox" id="1stars"><span> <i class="fa fa-star"></i> 1 STAR</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-block btn-success searchSubject" type="button" id="searchSubject2">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-9">
                <br>
                <!-- ==== TEAM MEMBERS ==== -->
                    <div class="container-fluid" id="team" name="team" style="margin-top:-35px">
                    </div>
                <!-- TEAM MEMBERS -->
            </div>   
        </div>
    </div>
    <div style="display:none">
        <form id="hForm">
            <input type="hidden" id="hZipcode">
            <input type="hidden" id="hLatitude">
            <input type="hidden" id="hLongitude">
        </form>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        $(function(){
            $(".select").select2();

            $.fn.initPage           =   function(){
                var subject_id      =   decodeURIComponent(getUrlParameter('subject')).split(',');
                var myGrade_id      =   decodeURIComponent(getUrlParameter('grade')).split(',');
                var zipcode         =   decodeURIComponent(getUrlParameter('zipcode')).split(',');
                var minPrice        =   decodeURIComponent(getUrlParameter('minPrice')).split(',');
                var maxPrice        =   decodeURIComponent(getUrlParameter('maxPrice')).split(',');
                var tutorRating     =   decodeURIComponent(getUrlParameter('tutorRating')).split(',');
                var distance        =   decodeURIComponent(getUrlParameter('distance')).split(',');
                //var tutorRating     =   decodeURIComponent(getUrlParameter('tutorRating')).split(',');

                $('#subject_id').val(subject_id).select2();
                $('#myGrade_id').val(myGrade_id).select2();
                //$('#zipcode').val(zipcode).select2();
                $('#zipcode').val(zipcode);
                $('#minPrice').val(minPrice);
                $('#maxPrice').val(maxPrice);
                $('#tutorRating').val(tutorRating);
                $("#distance").val(distance).select2();

                $.each(tutorRating,function(i,v){
                    $("#"+v+"stars").prop('checked', true);
                });

                $.fn.search();
            }

            $.fn.search             =   function(){
                var subject         =   $('#subject_id').val();
                var grade           =   $('#myGrade_id').val();
                var zipcode         =   $('#zipcode').val();
                var minPrice        =   $('#minPrice').val();
                var maxPrice        =   $('#maxPrice').val();
                var distance        =   $("#distance").val();
                //var tutorRating     =   $('#tutorRating').val();

                if(subject) 
                {
                    subject         = subject.toString();
                }
                else 
                {
                    subject         = null;
                }
                if(distance) 
                {
                    distance        = distance.toString();
                }
                else 
                {
                    distance         = null;
                }
                if(grade) 
                {
                    grade           = grade.toString();
                }
                else 
                {
                    grade           = null;
                }
                if(zipcode) 
                {
                    zipcode         = zipcode.toString();
                }
                else 
                {
                    zipcode         = "";
                }
                if(!minPrice)
                {
                    minPrice        = 0;
                }
                if(!maxPrice)
                {
                    maxPrice        = 15;
                }
                if(!tutorRating)
                {
                    tutorRating     = "0";
                }
                /*$("#rateYo").rateYo({
                    rating: tutorRating,
                    precision: 1,
                    fullStar : true,
                    onChange: function (rating, rateYoInstance) {
                        $("#tutorRating").val(rating);
                    }
                });
                var tutorRating =   $('#tutorRating').val();*/

                var tutorRating     =      "";

                for(var x = 1; x <= 5; x++)
                {
                    if($("#"+x+"stars").is(':checked'))
                    {
                        tutorRating =   x +"," + tutorRating;
                    }
                }

                if(tutorRating.length > 0)
                {
                    tutorRating     =   tutorRating.substring(0,(tutorRating.length-1));
                }

                window.history.pushState({"html": '',"pageTitle": 'Searching'},"Search State", 'searchTutor?subject=' + encodeURIComponent(subject) + '&grade=' + encodeURIComponent(grade) + '&zipcode=' + encodeURIComponent(zipcode) + '&minPrice=' + encodeURIComponent(minPrice) + '&maxPrice=' + encodeURIComponent(maxPrice) + '&tutorRating=' + encodeURIComponent(tutorRating) + '&distance=' + encodeURIComponent(distance) );
                var data        =   {'_token':'{{csrf_token()}}'};
                data.subject    =   subject;
                data.grade      =   grade;
                data.zipcode    =   zipcode;
                data.minPrice   =   minPrice;
                data.maxPrice   =   maxPrice;
                data.distance   =   distance;
                data.tutorRating=   (tutorRating);
                data            =   JSON.stringify(data);

                if(zipcode.toString().length > 0)
                {
                    var data2       =   {'_token':'{{csrf_token()}}'};
                    data2.zipcode   =   zipcode;
                    data2           =   JSON.stringify(data2);
                    //console.log(data);
                    $.ajax({
                        contentType : 'application/json',
                        type        :  'POST',
                        url         : '{{ url("/user/checkZipcode") }}',
                        data        :  data2,
                        complete: function(xhr){
                        },
                        success : function(data){
                            if(data.isZipcode == 0)
                            {
                                $.fn.getLatLong(zipcode);
                            }
                        }
                    });    
                }
                
                $.ajax({
                    contentType : 'application/json',
                    type        :  'POST',
                    url         : '{{ url("/home/searchTutorResult") }}',
                    data        :   data,
                    complete: function(xhr){
                        if(xhr.status==422)
                        {
                            response = JSON.parse(xhr.responseText);
                            var errors = [];
                            $.each(response, function(i, v) {
                                $.each(v, function(x, e) {
                                    errors.push(e);
                                });
                            });
                            var html = displayErrors(errors);
                            $("#team").html(html);
                            $("#searchSubject").removeAttr('disabled');
                        } 
                    },
                    success : function(data){
                        $(".searchSubject").removeAttr('disabled');
                        var html = "<div class='row white centered'>";
                        if(data.result == 0)
                        {
                            if(data.noUser == 0)
                            {
                                html    +=  "<h2>No Tutor Found.</h2>";    
                            }
                            else
                            {
                                html    +=  "<h2>No Tutor Found. Showing Related Tutors. Use Advanced Filters To Find Tutors Near You</h2>";
                            }
                        }
                        else if(data.result == 1)
                        {
                            html    +=  "<h2>Search Result</h2>";
                        }
                        html        +=  "<hr>";
                        var j = 0;
                        $.each(data.tutors, function(key,tutor){
                            html    +=  "<div class='col-lg-4 centered'>"                                                                   +
                                            "<a href='{{url('/user/profile')}}/"+tutor.id+"'>"                                              +
                                                "<img class='img img-circle' src='"+tutor.image+"' height='100px' width='100px' alt=''>"    +
                                                "<br>"                                                                                      +
                                                "<h4><b>"+tutor.name+"</b></h4>"                                                            +
                                                "<h5><b>"+tutor.myGrade+"</b></h5>";
                                                /*$.each(tutor.subjects, function(key1,subject){
                                                    html    +=  "<i class='fa fa-circle' aria-hidden='true' style='font-size:x-small'></i> "+subject.userSubject+"&nbsp;&nbsp;";
                                                });*/
                                                html += "<h5 style='margin-left:35px' id='rateYo"+key+"' class='centered'>"                 +
                                                "</h5>"                                                                                     +
                                                "<input type='hidden' id='rating"+key+"' value='"+tutor.avgRating+"'>"                      +
                                            "</a>"                                                                                          +
                                        "</div>";
                            j   =   key;
                        });

                        html    +=  "</div>";

                        $("#team").html(html);

                        for(var i=0;i<=j;i++)
                        {
                            $("#rateYo"+i).rateYo({
                                rating: $("#rating"+i).val(),
                                precision: 1,
                                readOnly: true,
                                fullStar : true
                            });   
                        }
                    }
                });
            }

            $.fn.initPage();

            $(".searchSubject").on("click",function(e){
                $(this).prop('disabled', true);
                e.preventDefault();
                $.fn.search();
            });

            $.fn.getLatLong     =   function(zipcode){
                $.ajax({
                   url : "https://maps.googleapis.com/maps/api/geocode/json?components=postal_code:"+zipcode+"&sensor=false",
                   method: "POST",
                   success:function(data){
                       latitude = data.results[0].geometry.location.lat;
                       longitude= data.results[0].geometry.location.lng;
                       //alert("Lat = "+latitude+"- Long = "+longitude);
                        $("#hLatitude").val(latitude.toString());
                        $("#hLongitude").val(longitude.toString());
                        $("#hZipcode").val(zipcode.toString());
                        $.fn.addZipCode();
                   }
                });
            }

            $.fn.addZipCode     =   function(){
                var data        =   {'_token':'{{csrf_token()}}'};
                data.zipcode    =   $("#hZipcode").val();
                data.latitude   =   $("#hLatitude").val();
                data.longitude  =   $("#hLongitude").val();
                data            =   JSON.stringify(data);

                $.ajax({
                    contentType : 'application/json',
                    type        :  'POST',
                    url         : '{{ url("/user/addZipcode") }}',
                    data        :  data,
                    complete: function(xhr){
                    },
                    success : function(data){
                        console.log("Zipcode Added!");
                    }
                });
            }
            
        });
    </script>
@stop

