@extends('frontend.master')

@section('title')
    <title>Contact Us</title>
@stop

@section('contents')
    <div class="container" style="margin-bottom:50px;margin-top:50px">
        <br>
        @if(count($errors))
            @foreach($errors->all() as $error)
            <div class="row alert alert-danger" id="errorDiv">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{$error}}
            </div>
            @endforeach
        @endif
    </div>
    <div class="container" id="contact" name="contact">
        <br>
        <div class="row">
            <h1 class="centered">THANKS FOR VISITING US</h1>
            <hr>
            <h4 class="centered">Feel free to contact us with questions, suggestions, or comments.</h4>
            <div class="col-lg-6 col-lg-offset-3">
                <form id="signUpFrm" method="POST" action="{{ url('/user/contact') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for=“comments”>Comments</label>
                            <textarea id='comments' cols='30' rows='3' class='form-control' name="comments">
                            </textarea>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-block btn-success" value="Submit">
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <div class="row">
        <br><br>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        $(function(){
            $("#contactUs").addClass("active");            
        });
    </script>
@stop