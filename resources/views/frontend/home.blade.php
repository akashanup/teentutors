@extends('frontend.master')

@section('title')
    <title>Home</title>
@stop

@section('contents')
    <div class="container" style="margin-bottom:50px;margin-top:50px">
        <br>
        @if(count($errors))
            @foreach($errors->all() as $error)
            <div class="row alert alert-danger" id="errorDiv">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{$error}}
            </div>
            @endforeach
        @endif
    </div>
    <!-- ==== HEADERWRAP ==== -->
        <div id="headerwrap" name="home">
            <header class="clearfix">
                <p>
                    <div class="row">
                        <div class="col-lg-3 col-lg-offset-2 col-md-3 col-md-offset-2 col-sm-6 col-xs-6">
                            <span class="btn btn-lg btn-block btn-primary find" data-id="2"><b style="font-size:x-large"> Find a Student</b></span>
                        </div>
                        <div class="col-lg-2 col-md-2">
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <span class="btn btn-lg btn-block btn-primary find" data-id="1"><b style="font-size:x-large"> Find a Tutor</b></span>
                        </div>    
                    </div>
                    <br>
                    <div class="row" id="fieldDiv">
                        <form id="searchFrm" role="form" style="visibility:hidden">
                            <div class="form-group">
                                <div class="col-lg-3 col-lg-offset-1 col-md-3 col-md-offset-1 col-sm-6 col-xs-6">
                                    <label for="subject_id">Choose Subject(s)</label>
                                    <select id="subject_id" class="select form-control" multiple="multiple">
                                        @foreach($subjects as $subject)
                                            <option value="{{$subject->id}}">{{$subject->subject}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                    <label for="zipcode">Choose Zipcode</label>
                                    <input type="text" class="form-control" id="zipcode">
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                                    <label for="search">&nbsp;</label>
                                    <input type="button" value="Search" class="btn btn-primary btn-block" id="search">
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
                                    <label for="cancel">&nbsp;</label>
                                    <input type="button" value="Cancel" class="btn btn-danger btn-block" id="cancel">
                                </div>
                            </div>
                        </form>    
                    </div>
                </p>
            </header>       
        </div>
    <!-- /Headerwrap -->

    <!-- ==== TEAM MEMBERS ==== -->
        <div class="container" id="team" name="team" style="margin-top:-35px">
            <br>
            <div class="row white centered">
                <h1 class="centered">RECOMMENDED TUTORS</h1>
                <hr>
                <br>
                <br>
                @foreach($users as $key => $user)
                    <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12 centered">
                        <a href="{{url('/user/profile').'/'.$user->id}}">
                            <img class="img img-circle" src="{{ $user->image }}" height="120px" width="120px" alt="">
                            <br>
                            <h4><b>{{$user->name}}</b></h4>
                            <h5 style="margin-left:45px" id="rateYo{{$key}}">
                            </h5>
                            <input type="hidden" id="rating{{$key}}" value="{{$user->avgRating}}">
                        </a>
                    </div>
                @endforeach
            </div>
            <br>
        </div>
    <!-- TEAM MEMBERS -->

    <!-- ==== GREYWRAP ==== -->
        <div id="greywrap">
            <!-- Begin services-section -->
                <section id="services" class="services-section section-global-wrapper">
                    <div class="container">          
                        <!-- Begin Services Row 1 -->
                            <div class="row services-row services-row-head services-row-1">
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="services-group wow animated fadeInLeft" data-wow-offset="40">
                                        <div class="row" id="dollarBefore">
                                            <p class="services-icon"><span class="fa fa-usd fa-5x" style="color:#1BBD9B"></span></p>
                                            <h2 style="color:white">
                                                &nbsp;&nbsp;Tutors at your price
                                            </h2>
                                            <a class="pull-right" id="dollarMore" style="margin-right:5px">View More</a>
                                        </div>
                                        <div class="row" style="display:none" id="dollarAfter">
                                            <p class="services-icon"><span class="fa fa-usd fa-3x" style="color:#1BBD9B"></span></p>
                                            <p style="font-size:large;text-align:center;padding:5px">
                                                High school tutors who can charge up to $15 are much more affordable than the average tutor who charges around $50 an hour.<br><br>
                                            </p>
                                            <a class="pull-right" id="dollarLess" style="margin-top:-25px;margin-right:5px">View Less</a>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="services-group wow animated zoomIn" data-wow-offset="40">
                                        <div class="row" id="smileBefore">
                                            <p class="services-icon"><span class="fa fa-smile-o fa-5x" style="color:#1BBD9B"></span></p>
                                            <h2 style="color:white">
                                                &nbsp;&nbsp;Learning can be fun
                                            </h2>
                                            <a class="pull-right" id="smileMore" style="margin-right:5px">View More</a>
                                        </div>
                                        <div class="row" style="display:none" id="smileAfter">
                                            <p class="services-icon"><span class="fa fa-smile-o fa-3x" style="color:#1BBD9B"></span></p>
                                            <p style="font-size:large;text-align:center;padding:5px">
                                                High schoolers can easily relate to the student and incorporate more modern and interesting method’s in their teaching to maximize the student’s learning.
                                            </p>
                                            <a class="pull-right" id="smileLess" style="margin-top:-25px;margin-right:5px">View Less</a>
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="services-group wow animated fadeInRight" data-wow-offset="40">
                                        <div class="row" id="thumbsUpBefore">
                                            <p class="services-icon"><span class="fa fa-thumbs-o-up fa-5x" style="color:#1BBD9B"></span></p>
                                            <h2 style="color:white">
                                                &nbsp;Choose from experts
                                            </h2>
                                            <a class="pull-right" id="thumbsUpMore" style="margin-right:5px">View More</a>
                                        </div>
                                        <div class="row" style="display:none" id="thumbsUpAfter">
                                            <p class="services-icon"><span class="fa fa-thumbs-o-up fa-3x" style="color:#1BBD9B"></span></p>
                                            <p style="font-size:large;text-align:center;padding:5px">
                                                High schoolers have recently mastered what they are tutoring and may be reviewing this material for standardized exams.
                                            </p>
                                            <a class="pull-right" id="thumbsUpLess" style="margin-top:-25px;margin-right:5px">View Less</a>
                                        </div>
                                    </div>        
                                </div>
                            </div>
                        <!-- End Serivces Row 1 -->        
                    </div>      
                </section>
            <!-- End services-section -->
        </div>
    <!-- greywrap -->

        
@stop

@section('scripts')
    <script type="text/javascript">
        $(function(){
            $("#home").addClass("active");

            $(".select").select2();
            
            for(var i=0;i<4;i++)
            {
                $("#rateYo"+i).rateYo({
                    rating: $("#rating"+i).val(),
                    precision: 1,
                    readOnly: true,
                    fullStar : true
                });   
            }

            $("#dollarMore").on("click",function(){
                $("#dollarBefore").css("display","none");
                $("#dollarAfter").css("display","block");
            });
            $("#dollarLess").on("click",function(){
                $("#dollarBefore").css("display","block");
                $("#dollarAfter").css("display","none");
            });

            $("#smileMore").on("click",function(){
                $("#smileBefore").css("display","none");
                $("#smileAfter").css("display","block");
            });
            $("#smileLess").on("click",function(){
                $("#smileBefore").css("display","block");
                $("#smileAfter").css("display","none");
            });

            $("#thumbsUpMore").on("click",function(){
                $("#thumbsUpBefore").css("display","none");
                $("#thumbsUpAfter").css("display","block");
            });
            $("#thumbsUpLess").on("click",function(){
                $("#thumbsUpBefore").css("display","block");
                $("#thumbsUpAfter").css("display","none");
            });
            
            var data_id =   "";
            $(".find").on("click",function(){
                //$("#btnDiv").css("display","none");
                $(".find").removeClass("btn-success");
                $(".find").addClass("btn-primary");
                $(this).addClass("btn-success");
                $("#searchFrm").css("visibility","visible");
                data_id     =   $(this).attr("data-id");

            });

            $("#cancel").on("click",function(){
                //$("#btnDiv").css("display","block");
                $(".find").removeClass("btn-success");
                $(".find").removeClass("btn-primary");
                $(".find").addClass("btn-primary");
                $("#searchFrm").css("visibility","hidden");
            });

            $("#search").on("click",function(){
                var subject_id  =   $("#subject_id").val();//.toString();
                var zipcode     =   $("#zipcode").val();//.toString();
                if(data_id == 1)
                {
                    window.location =  "{{url('/home/searchTutor')}}?subject="+subject_id+"&grade=null&zipcode="+zipcode+"&minPrice=0&maxPrice=15&tutorRating=null";
                }
                else if(data_id == 2)
                {
                    window.location =  "{{url('/home/searchStudent')}}?subject="+subject_id+"&grade=null&zipcode="+zipcode;
                }
            })
        });
    </script>
@stop