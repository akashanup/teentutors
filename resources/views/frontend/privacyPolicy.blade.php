@extends('frontend.master')

@section('title')
    <title>Privacy Policy</title>
@stop

@section('contents')
    <div class="container" style="margin-bottom:50px;margin-top:50px">
        <br>
        @if(count($errors))
            @foreach($errors->all() as $error)
            <div class="row alert alert-danger" id="errorDiv">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{$error}}
            </div>
            @endforeach
        @endif
    </div>
    <div class="container">
        <br>
        <div class="row">
            <h1>Privacy Policy</h1>
            <div class="row">
                This privacy policy has been compiled to better serve those who are concerned with how their 'Personally Identifiable Information' (PII) is being used online. PII, as described in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.
            </div>

            <h3>What personal information do we collect from the people that visit our blog, website or app?</h3>

            <div class="row">
                We do not collect information from visitors of our site.
                Zip code, email, parent’s email, optional photo, optional zip code, grade, subjects you want to teach, grades you are interested in teaching, price, comments, subjects you want to study, grades tutor should be in, or other details to help you with your experience.
            </div>

            <h3>When do we collect information?</h3>

            <div class="row">
                We collect information from you when you register on our site, respond to a survey, fill out a form or enter information on our site.
                Enter zip code,subject, and advanced filters 
            </div>

            <h3>How do we use your information?</h3>

            <div class="row">
                We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:
                <br>
              • To personalize your experience and to allow us to deliver the type of content and product offerings in which you are most interested.<br>
              • To improve our website in order to better serve you.<br>
              • To ask for ratings and reviews of services or products.<br>
              • To follow up with them after correspondence (live chat, email or phone inquiries).<br>
            </div>

            <h3>How do we protect your information?</h3>

            <div class="row">
                We do not use vulnerability scanning and/or scanning to PCI standards.
                We only provide articles and information. We never ask for credit card numbers.
                We do not use Malware Scanning.

                Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology. 

                We implement a variety of security measures when a user places an order enters, submits, or accesses their information to maintain the safety of your personal information.

                For your convenience we may store your credit card information kept for more than 60 days in order to expedite future orders, and to automate the billing process.

            </div>

            <h3>Do we use 'cookies'?</h3>

            <div class="row">
                Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.

                We use cookies to compile aggregate data about site traffic and site interactions in order to offer better site experiences and tools in the future. We may also use trusted third-party services that track this information on our behalf.

                You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look at your browser's Help Menu to learn the correct way to modify your cookies.

                If you turn cookies off, some features will be disabled. It won't affect the user's experience that make your site experience more efficient and may not function properly.

                However, you will still be able to place orders .
            </div>
            <br>
            <h2>Third-party disclosure</h2>

            <h3>Do we disclose the information we collect to Third-Parties?</h3>
            <div class="row">
                We sell,trade, or otherwise transfer to outside parties your name, address,city,town, any form or online contact identifier email, name of chat account etc., screen name or user names, others
                Zip code, optional photo, optional zip code, grade, subjects you want to teach, grades you are interested in teaching, price, comments, subjects you want to study, grades tutor should be in
                Personally Identifiable Information.
            </div>

            <h3>Third-party links</h3>

            <div class="row">
                Occasionally, at our discretion, we may include or offer third-party products or services on our website. These third-party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.
            </div>                

            <h3>Google</h3>

            <div class="row">
                Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users. https://support.google.com/adwordspolicy/answer/1316548?hl=en 

                We have not enabled Google AdSense on our site but we may do so in the future.

                California Online Privacy Protection Act

                CalOPPA is the first state law in the nation to require commercial websites and online services to post a privacy policy. The law's reach stretches well beyond California to require any person or company in the United States (and conceivably the world) that operates websites collecting Personally Identifiable Information from California consumers to post a conspicuous privacy policy on its website stating exactly the information being collected and those individuals or companies with whom it is being shared. - See more at: http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf

                According to CalOPPA, we agree to the following:
                Users can visit our site anonymously.
                Once this privacy policy is created, we will add a link to it on our home page or as a minimum, on the first significant page after entering our website.
                Our Privacy Policy link includes the word 'Privacy' and can easily be found on the page specified above.

                You will be notified of any Privacy Policy changes:
                      • On our Privacy Policy Page can change your personal information.<br>
                      • By logging in to your account.
            </div>

            <h3>How does our site handle Do Not Track signals?</h3>

            <div class="row">
                We honor Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place. 
            </div>                

            <h3>Does our site allow third-party behavioral tracking?</h3>
            <div class="row">
                It's also important to note that we do not allow third-party behavioral tracking.
            </div>

            <h3>Privacy Policy:</h3>
            <div class="row">
                In the privacy policy: mention:
                Disclaimer: Theme created by Carlos from Blacktie.co
                Provide link: http://blacktie.co/2014/02/shield-one-page-theme/
                Copyright blacktie.co
            </div>

            <h3>COPPA (Children Online Privacy Protection Act)</h3>

            <div class="row">   
                When it comes to the collection of personal information from children under the age of 13 years old, the Children's Online Privacy Protection Act (COPPA) puts parents in control. The Federal Trade Commission, United States' consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do to protect children's privacy and safety online.

                We market to and collect information from children under 13.
                We also allow third-parties, including ad networks or plug-ins collect Personally Identifiable Information from children under 13. 
            </div>

            <h3>
                In order to remove your child's information please contact the following personnel: 
            </h3>

            <div class="row">
                Nimish Garg
                nimishg@bu.edu
                Anybody in the public
            </div>

            <h3>We adhere to the following COPPA tenants: </h3>
            <div class="row">
                • We will not require a child to disclose more infomration than is reasonably necessary to particapate in an activity.<br>
                • Parents can review their child's personal information, direct us to delete it, and refuse to allow any further collection or use of the child's information.<br>
                • Parents can agree to the collection and use of their child's information, but still not allow disclosure to third-parties unless that's part of the service.<br>
                • Parents can review, delete, manage or refuse with whom their child's information is shared through through emailing our support staff contacting us directly.<br>
                • We will notify parents directly before collecting PII from their kids. This includes what specific information you will want to collect and how it might be disclosed, a link to your online privacy policy, and how the parents can give their consent. Also, if the parent doesn't consent within a reasonable time, you will delete the parent's and child's online contact info from your records.<br>
                • Parents can give consent by or contacting us directly.
            </div>

            <h3>Fair Information Practices</h3>

            <div class="row">
                The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented is critical to comply with the various privacy laws that protect personal information.

                <b>In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:</b>
                We will notify the users via in-site notification within 7 business days.

                We also agree to the Individual Redress Principle which requires that individuals have the right to legally pursue enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals have recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.

                <b>Waiver of Liability</b>
                This agreement releases Teen Tutors from all liability relating to injuries that may occur during tutoring. By signing this agreement, I agree to hold Teen Tutors entirely free from any liability, including financial responsibility for injuries incurred, regardless of whether injuries are caused by negligence.
                I swear that I am participating voluntarily. Additionally, I do not have any conditions that will increase my likelihood of experiencing injuries while engaging in this activity.
                By confirming this account, I forfeit all right to bring a suit against Teen Tutors for any reason. In return, I will be able to put my information, my parent’s information, and my child’s information. I will also make every effort to obey safety precautions as listed in writing and as explained to me verbally. I will ask for clarification when needed.
            </div>
        </div>
    </div>
    <div class="row">
        <br><br>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        $(function(){
        });
    </script>
@stop