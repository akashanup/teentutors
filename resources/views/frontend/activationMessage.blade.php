@extends('frontend.master')

@section('title')
    <title>Account Activation Message</title>
@stop

@section('contents')
    <div class="container" style="margin-bottom:50px;margin-top:50px">
        <br>
        @if(count($errors))
            @foreach($errors->all() as $error)
            <div class="row alert alert-danger" id="errorDiv">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{$error}}
            </div>
            @endforeach
        @endif
    </div>

    <div class="container">
        <div class="row" style="text-align:center;margin-top:100px;margin-bottom:225px">
            <h1>Account activation link has been sent to your parent's email address. </h1>
            <h1>Ask your parent to activate your account.</h1>
            <br>
            <h5>Please check your junk email if you do not receive the confirmation in your Inbox.</h5>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        $(function(){            
        });
    </script>
@stop