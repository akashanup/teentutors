@extends('frontend.master')

@section('title')
    <title>About Us</title>
@stop

@section('contents')
    <div class="container" style="margin-bottom:50px;margin-top:50px">
        <br>
        @if(count($errors))
            @foreach($errors->all() as $error)
            <div class="row alert alert-danger" id="errorDiv">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{$error}}
            </div>
            @endforeach
        @endif
    </div>

    <!-- ==== SECTION DIVIDER1 -->
        <section class="section-divider textdivider divider1" style="margin-top:-71px;">
        </section>
        <div class="container">
            <h1 style="text-shadow: 2px 2px 2px #000;letter-spacing: 8px;color:orange;font-size: 30px;text-align:center"><b>ABOUT US</b></h1>
            <hr>
            <h3 style="text-shadow: 1px 1px 1px #000;letter-spacing: 4px; color:orange;font-size: 20px;text-align:center"><b>Team of Tutors For You With You</b></h3>
            <p style="color:black;font-weight: bold;line-height:35px;letter-spacing: 1px;">
                Teen Tutors is a non-profit site that serves to connect high school tutors with students to provide affordable, high-quality education without any middleman fees. Enthusiastic tutors will work with students to maximize their potential. They will provide homework help, test prep, or anything your child needs to succeed, and they will incorporate teaching methods that work best with children.
            </p>
        </div>
    <!-- section divider1 -->

    <div class="container-fluid" id="blog" name="blog" style="background-color:#F2F2F2">
            <div class="container">
                <div class="row">
                    <h1 class="centered">About the Founder</h1>
                    <hr>               
                </div>
                <div class="row">
                    <div class="col-lg-2 col-sm-3 col-sm-12 col-xs-12">
                        <img src="{{ asset('/frontend/assets/img/nimish.png') }}" class="img-responsive" height="25px" width="125px">
                    </div>
                    <div class="col-lg-10 col-sm-9 col-sm-12 col-xs-12">
                        <p style="color:black;font-weight: bold;line-height:35px;letter-spacing: 1px;">
                           Nimish Garg is a junior at BU Academy. He is a keen entrepeneur, helping students find opportunities such as by running a Model UN conference called BUAMUN (buamun.org), teaching other students about the stock market at his school, and starting a hackathon for high schoolers.
                        </p>
                    </div>
                </div>
           </div>
    </div>

    <!-- ==== BLOG ==== -->
        <div class="container-fluid" id="blog" name="blog" style="background-color:#F9F9F9">
            <br>
            <div class="container">
                <div class="row">
                    <h1 class="centered">FAQ</h1>
                    <hr>
                    <br>
                </div>
                
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table-responsive" style="font-size:larger">
                            <tr>
                                <th>Q:</th>
                                <td>Will the tutor drive to my house or will I have to travel?
                                </td>
                            </tr>
                            <tr>
                                <th>A:</th>
                                <td>This depends on tutor's individual policy. To learn more about a tutor, you may email him/her.
                                </td>
                            </tr>
                            <tr>
                                <th><br></th>
                                <td><br>
                                </td>
                            </tr>
                            <tr>
                                <th>Q:</th>
                                <td>What subjects can i get help with?
                                </td>
                            </tr>
                            <tr>
                                <th>A:</th>
                                <td>We offer more then 30 subjects available for tutors to teach. Please search for the subject you are interested in through the search box.
                                </td>
                            </tr>
                            <tr>
                                <th><br></th>
                                <td><br>
                                </td>
                            </tr>
                            <tr>
                                <th>Q:</th>
                                <td>Are there any refunds?
                                </td>
                            </tr>
                            <tr>
                                <th>A:</th>
                                <td>Teen Tutors is a non-profit website and simply connects tutors with students. Therefore, we cannot provide refunds.
                                </td>
                            </tr>
                            <tr>
                                <th><br></th>
                                <td><br>
                                </td>
                            </tr>
                            <tr>
                                <th>Q:</th>
                                <td>Who do I contact if I have any more questions?
                                </td>
                            </tr>
                            <tr>
                                <th>A:</th>
                                <td>Please feel free to email <b>nimishg@bu.edu</b> if you have any questions, comments or suggestions.
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <br>
    <!-- ==== BLOG ==== -->        
        
@stop

@section('modals')
    
@stop

@section('scripts')
    <script type="text/javascript">
        $(function(){
            $("#aboutUs").addClass("active");
        });
    </script>
@stop