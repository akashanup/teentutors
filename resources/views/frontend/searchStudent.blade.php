@extends('frontend.master')

@section('title')
    <title>Student</title>
@stop

@section('contents')
    <div class="container" style="margin-bottom:50px;margin-top:50px">
        <br>
        @if(count($errors))
            @foreach($errors->all() as $error)
            <div class="row alert alert-danger" id="errorDiv">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{$error}}
            </div>
            @endforeach
        @endif
    </div>
    <div class="container" style="margin-bottom:175px">
        <div class="row" style="margin-top:-60px">
            <h2>Search Here</h2>
            <form>
                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label for="subject_id">Subject(s)</label>
                        <select name="subject_id" id="subject_id" class="select form-control" multiple="multiple">
                            @foreach($subjects as $subject)
                                    <option value="{{$subject->id}}">{{$subject->subject}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-6 col-xs-6">
                    <div class="form-group">
                        <label for="zipcode">Zipcode</label>
                        <input type="text" name="zipcode" class="form-control" id="zipcode">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="searchSubject1">&nbsp;</label>
                        <button class="btn btn-block btn-success searchSubject" type="button" id="searchSubject1"><i class="fa fa-search" area-hidden="true"></i> Search</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="row">
            <br><br>
            <div class="col-lg-3">
                    <form id="searchFrm">
                        <div class="row">
                            <h4><b>Advanced Filters</b></h4>
                            
                            <div class="form-group">
                                <label for="distance">Distance</label>
                                <select name="distance" id="distance" class="select form-control">
                                    <option value="0">None</option>
                                    <option value="1">Up to 5 Miles</option>
                                    <option value="2">Up to 10 Miles</option>
                                    <option value="3">Up to 15 Miles</option>
                                    <option value="4">Up to 20 Miles</option>
                                </select>
                            </div>
                            <div class='form-group'>
                                <div class='row'>
                                    <label style='margin-left:15px'>Grades Of Student</label><br>
                                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'>
                                        <label for='from'>From</label>
                                        <select name='from' id='from' class='select form-control'>
                                            @foreach($grades as $key=>$grade)
                                                <option value="{{$grade->id}}">{{$grade->grade}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'>
                                        <label for='to'>To</label>
                                        <select name='to' id='to' class='select form-control'>
                                            @foreach($grades as $key=>$grade)
                                                @if($key>0)
                                                    <option value="{{$grade->id}}">{{$grade->grade}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-block btn-success searchSubject" type="button" id="searchSubject2">Filter</button>
                            </div>
                        </div>
                    </form>
            </div>
            <!-- ==== TEAM MEMBERS ==== -->
                <br>
                <div class="col-lg-9">
                    <div class="container-fluid" id="team" name="team" style="margin-top:-35px">
                    </div>
                </div>
            <!-- TEAM MEMBERS -->
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
    </div>
    <div style="display:none">
        <form id="hForm">
            <input type="hidden" id="hZipcode">
            <input type="hidden" id="hLatitude">
            <input type="hidden" id="hLongitude">
        </form>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        $(function(){
            $(".select").select2();

            $.fn.initPage           =   function(){
                var subject_id      =   decodeURIComponent(getUrlParameter('subject')).split(',');
                var myGrade_id      =   decodeURIComponent(getUrlParameter('grade'));//.split(',');
                var zipcode         =   decodeURIComponent(getUrlParameter('zipcode')).split(',');
                var distance        =   decodeURIComponent(getUrlParameter('distance')).split(',');
                $('#subject_id').val(subject_id).select2();
                //$('#myGrade_id').val(myGrade_id).select2();
                $('#zipcode').val(zipcode);
                $("#to").val(myGrade_id.charAt(0)).select2();
                $("#from").val(myGrade_id.charAt(myGrade_id.length - 1)).select2();
                $("#distance").val(distance).select2();
                $.fn.search();
            }

            $.fn.search             =   function(){
                var subject         =   $('#subject_id').val();
                //var grade           =   $('#myGrade_id').val();
                var grade           =   "";
                var zipcode         =   $('#zipcode').val();
                var from            =   $("#from").val();
                var to              =   $("#to").val();
                var distance        =   $("#distance").val();

                for(var i = from; i <= to; i++)
                {
                    grade = i+","+grade;
                }
                var n       =   grade.lastIndexOf(",");
                grade       =   grade.substring(0,n);
                if(grade == "null")
                {
                    grade     =   null;
                }

                if(subject) 
                {
                    subject         = subject.toString();
                }
                else 
                {
                    subject         = null;
                }
                
                if(distance) 
                {
                    distance        = distance.toString();
                }
                else 
                {
                    distance         = null;
                }
                /*if(grade) 
                {
                    grade           = grade.toString();
                }
                else 
                {
                    grade           = null;
                }*/
                if(zipcode) 
                {
                    zipcode         = zipcode.toString();
                }
                else 
                {
                    zipcode         = "";
                }
                
                window.history.pushState({"html": '',"pageTitle": 'Searching'},"Search State", 'searchStudent?subject=' + encodeURIComponent(subject) + '&grade=' + encodeURIComponent(grade) + '&zipcode=' + encodeURIComponent(zipcode) + '&distance=' + encodeURIComponent(distance) );
                var data        =   {'_token':'{{csrf_token()}}'};
                data.subject    =   subject;
                data.grade      =   grade;
                data.zipcode    =   zipcode;
                data.distance   =   distance;
                data            =   JSON.stringify(data);
                //console.log(data);

                if(zipcode.toString().length > 0)
                {
                    var data2       =   {'_token':'{{csrf_token()}}'};
                    data2.zipcode   =   zipcode;
                    data2           =   JSON.stringify(data2);
                    //console.log(data);
                    $.ajax({
                        contentType : 'application/json',
                        type        :  'POST',
                        url         : '{{ url("/user/checkZipcode") }}',
                        data        :  data2,
                        complete: function(xhr){
                        },
                        success : function(data){
                            if(data.isZipcode == 0)
                            {
                                $.fn.getLatLong(zipcode);
                            }
                        }
                    });    
                }

                $.ajax({
                    contentType : 'application/json',
                    type        :  'POST',
                    url         : '{{ url("/home/searchStudentResult") }}',
                    data        :   data,
                    complete: function(xhr){
                        if(xhr.status==422)
                        {
                            response = JSON.parse(xhr.responseText);
                            var errors = [];
                            $.each(response, function(i, v) {
                                $.each(v, function(x, e) {
                                    errors.push(e);
                                });
                            });
                            var html = displayErrors(errors);
                            $("#team").html(html);
                            $("#searchSubject").removeAttr('disabled');
                        } 
                    },
                    success : function(data){
                        $(".searchSubject").removeAttr('disabled');
                        var html = "<div class='row white centered'>";
                        if(data.result == 0)
                        {
                            if(data.noUser == 0)
                            {
                                html    +=  "<h2>No Student Found.</h2>";    
                            }
                            else
                            {
                                html    +=  "<h2>No Student Found. Showing Related Students. Use Advanced Filters To Find Students Near You</h2>";
                            }
                        }
                        else if(data.result == 1)
                        {
                            html    +=  "<h2>Search Result</h2>";
                        }
                        html        +=  "<hr>";
                        var j = 0;
                        $.each(data.tutors, function(key,tutor){
                            html    +=  "<div class='col-lg-4 centered'>"                                                                   +
                                            "<a href='{{url('/user/profile')}}/"+tutor.id+"'>"                                              +
                                                "<img class='img img-circle' src='"+tutor.image+"' height='100px' width='100px' alt=''>"    +
                                                "<br>"                                                                                      +
                                                "<h4><b>"+tutor.name+"</b></h4>"                                                            +
                                                "<h5><b>"+tutor.myGrade+"</b></h5>";
                                                /*$.each(tutor.subjects, function(key1,subject){
                                                    html    +=  "<i class='fa fa-circle' aria-hidden='true' style='font-size:x-small'></i> "+subject.userSubject+"&nbsp;&nbsp;";
                                                });*/
                                                html    +=  "</a>"                                                                           +
                                        "</div>";
                        });

                        html    +=  "</div>";

                        $("#team").html(html);
                    }
                });
            }
            
            $.fn.initPage();

            $.fn.getLatLong     =   function(zipcode){
                $.ajax({
                   url : "https://maps.googleapis.com/maps/api/geocode/json?components=postal_code:"+zipcode+"&sensor=false",
                   method: "POST",
                   success:function(data){
                       latitude = data.results[0].geometry.location.lat;
                       longitude= data.results[0].geometry.location.lng;
                       //alert("Lat = "+latitude+"- Long = "+longitude);
                        $("#hLatitude").val(latitude.toString());
                        $("#hLongitude").val(longitude.toString());
                        $("#hZipcode").val(zipcode.toString());
                        $.fn.addZipCode();
                   }
                });
            }

            $.fn.addZipCode     =   function(){
                var data        =   {'_token':'{{csrf_token()}}'};
                data.zipcode    =   $("#hZipcode").val();
                data.latitude   =   $("#hLatitude").val();
                data.longitude  =   $("#hLongitude").val();
                data            =   JSON.stringify(data);

                $.ajax({
                    contentType : 'application/json',
                    type        :  'POST',
                    url         : '{{ url("/user/addZipcode") }}',
                    data        :  data,
                    complete: function(xhr){
                    },
                    success : function(data){
                        console.log("Zipcode Added!");
                    }
                });
            }

            $(".searchSubject").on("click",function(e){
                $(this).prop('disabled', true);
                e.preventDefault();
                $.fn.search();
            });
            
        });
    </script>
@stop