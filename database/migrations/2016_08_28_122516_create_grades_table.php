<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('grade');
            $table->timestamps();
        });
        DB::table('grades')->insert([
            ['grade' => 'Pre-School'], 
            ['grade' => 'Kindergarten'],
            ['grade' => 'Grade I'],
            ['grade' => 'Grade II'],
            ['grade' => 'Grade III'],
            ['grade' => 'Grade IV'],
            ['grade' => 'Grade V'],
            ['grade' => 'Grade VI'],
            ['grade' => 'Grade VII'],
            ['grade' => 'Grade VIII'],
            ['grade' => 'Grade IX'],
            ['grade' => 'Grade X'],
            ['grade' => 'Grade XI'],
            ['grade' => 'Grade XII'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('grades');
    }
}
