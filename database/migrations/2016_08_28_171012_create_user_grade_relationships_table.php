<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGradeRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_grade_relationships', function (Blueprint $table) {
            $table  ->  increments('id');
            $table  ->  integer('user_id')->unsigned();
            $table  ->  integer('interestedGrades_id')->unsigned();
            $table  ->  timestamps();

            $table  ->  foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table  ->  foreign('interestedGrades_id')->references('id')->on('grades')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_grade_relationships');
    }
}
