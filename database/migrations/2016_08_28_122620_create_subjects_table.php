<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('subjects')->onDelete('cascade');
        });
        DB::table('subjects')->insert([
            ['subject' => 'English'                 ,   'parent_id' => null], 
            ['subject' => 'Reading'                 ,   'parent_id' => 1], 
            ['subject' => 'Writing'                 ,   'parent_id' => 1], 

            ['subject' => 'Math'                    ,   'parent_id' => null], 
            ['subject' => 'Arithmetic'              ,   'parent_id' => 2], 
            ['subject' => 'Algebra 1'               ,   'parent_id' => 2], 
            ['subject' => 'Algebra 2'               ,   'parent_id' => 2], 
            ['subject' => 'Geometry'                ,   'parent_id' => 2], 
            ['subject' => 'Pre Calculus'            ,   'parent_id' => 2], 
            ['subject' => 'Calculus'                ,   'parent_id' => 2], 
            
            ['subject' => 'Science'                 ,   'parent_id' => null], 
            ['subject' => 'Biology'                 ,   'parent_id' => 2], 
            ['subject' => 'Chemistry'               ,   'parent_id' => 2], 
            ['subject' => 'Physics'                 ,   'parent_id' => 2], 
            ['subject' => 'Environmental Science'   ,   'parent_id' => 2], 
            ['subject' => 'Earth Science'           ,   'parent_id' => 2],             
            
            ['subject' => 'Social Studies'          ,   'parent_id' => null],
            ['subject' => 'World History'           ,   'parent_id' => 3], 
            ['subject' => 'European History'        ,   'parent_id' => 3], 
            ['subject' => 'American History'        ,   'parent_id' => 3], 
            ['subject' => 'Geography'               ,   'parent_id' => 3],

            ['subject' => 'Languages'               ,   'parent_id' => null],
            ['subject' => 'Spanish'                 ,   'parent_id' => 4], 
            ['subject' => 'French'                  ,   'parent_id' => 4], 
            ['subject' => 'Latin'                   ,   'parent_id' => 4], 
            ['subject' => 'Greek'                   ,   'parent_id' => 4], 
            ['subject' => 'Mandarin'                ,   'parent_id' => 4], 
            ['subject' => 'Italian'                 ,   'parent_id' => 4], 
            ['subject' => 'Hindi'                   ,   'parent_id' => 4],    
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subjects');
    }
}
