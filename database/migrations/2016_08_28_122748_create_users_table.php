<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table  ->  increments('id');
            $table  ->  string('name');
            $table  ->  string('email')->unique();
            $table  ->  string('parents_email');
            $table  ->  string('password');
            $table  ->  string('comments',500);
            $table  ->  float('avgRating')->default(0.00);//only for tutors
            $table  ->  string('image');//->default("{{asset('/frontend/images/default.jpg')}}");
            $table  ->  string('resume')->nullable();//only for tutors
            $table  ->  integer('myGrade_id')->unsigned();
            $table  ->  integer('role_id')->unsigned();
            $table  ->  integer('price')->nullable();//only for tutors
            $table  ->  integer('status')->default(0);
            $table  ->  timestamps();

            $table  ->  foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table  ->  foreign('myGrade_id')->references('id')->on('grades')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
