<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZipcodeLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zipcode_locations', function (Blueprint $table) {
            $table  ->  increments('id');
            $table  ->  string('zipcode')->unique();
            $table  ->  string('latitude');
            $table  ->  string('longitude');
            $table  ->  timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('zipcode_locations');
    }
}
