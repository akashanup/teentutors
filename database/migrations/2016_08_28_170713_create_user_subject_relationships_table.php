<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSubjectRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_subject_relationships', function (Blueprint $table) {
            $table  ->  increments('id');
            $table  ->  integer('user_id')->unsigned();
            $table  ->  integer('subject_id')->unsigned();
            $table  ->  timestamps();

            $table  ->  foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table  ->  foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_subject_relationships');
    }
}
