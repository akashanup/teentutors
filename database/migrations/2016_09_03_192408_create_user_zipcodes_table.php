<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserZipcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_zipcodes', function (Blueprint $table) {
            $table  ->  increments('id');
            $table  ->  integer('user_id')->unsigned();
            $table  ->  string('zipcode');
            $table  ->  timestamps();
            $table  ->  foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_zipcodes');
    }
}
