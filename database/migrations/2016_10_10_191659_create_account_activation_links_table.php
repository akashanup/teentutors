<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountActivationLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_activation_links', function (Blueprint $table) {
            $table  ->  increments('id');
            $table  ->  integer('user_id')->unsigned();
            $table  ->  string('unique_key');
            $table  ->  timestamps();
            $table  ->  foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('account_activation_links');
    }
}
