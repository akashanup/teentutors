<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_ratings', function (Blueprint $table) {
            $table  ->  increments('id');
            $table  ->  integer('user_id')->unsigned();
            $table  ->  float('rating');
            $table  ->  string('reviews',500)->nullable();
            $table  ->  integer('rated_by')->unsigned()->nullable();
            $table  ->  timestamps();

            $table  ->  foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table  ->  foreign('rated_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_ratings');
    }
}
