function setError(element, message) {
	var parent = element.parents('.form-errors:eq(0)');
	if(parent.hasClass('has-error'))
		return;
	parent.addClass('has-error').append('<span class="help-block">' + message + '</span>')
}

function unsetError(element) {
	var parent = element.parents('.form-errors:eq(0)');
	parent.removeClass('has-error');	
	parent.find('.help-block').remove();
}

function displayErrors(errors) {
    var html="";
    $.each(errors, function(i, v) {
        html += "<div class='errorMessage alert alert-danger'>"                                                 +
                "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"  +
                    v                                                                                           +
                "</div>";                
    });
    return html;
}

function getUrlParameter(sParam) {
    sParam = sParam.toLowerCase();
    var sPageURL = window.location.search.substring(1).toLowerCase();
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}

$(function(){
    $('#zipcode').on("blur change keyup paste keypress keydown",function(e){
        if(!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode == 8) || (e.keyCode == 9) || (e.keyCode>=35 && e.keyCode <= 37) || (e.keyCode == 39) || (e.keyCode == 46) ))
        {   
            event.preventDefault();
            return false;                       
        }
    });

    $('#email').on('blur', function(){
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test($(this).val()))
        {
            setError($(this), "Please provide a valid email address.");
        }
        else
        {
            unsetError($(this));
        }
    });

    $('#password').on('blur', function(){
        var regex = /^(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;
        if(!regex.test($(this).val()))
        {
            setError($(this), "Password must be of atleast 8 characters with at least one caps and one digit.");
        }
        else
        {
            unsetError($(this));
        }
    });
});
