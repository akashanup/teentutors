function setError(element, message) {
	var parent = element.parents('.form-errors:eq(0)');
	if(parent.hasClass('has-error'))
		return;
	parent.addClass('has-error').append('<span class="help-block">' + message + '</span>')
}

function unsetError(element) {
	var parent = element.parents('.form-errors:eq(0)');
	parent.removeClass('has-error');	
	parent.find('.help-block').remove();
}

function displayErrors(errors) {
    var html="";
    $.each(errors, function(i, v) {
        html += "<div class='errorMessage alert alert-danger'>"                                                 +
                "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"  +
                    v                                                                                           +
                "</div>";                
    });
    return html;
}

function getUrlParameter(sParam) {
    sParam = sParam.toLowerCase();
    var sPageURL = window.location.search.substring(1).toLowerCase();
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}